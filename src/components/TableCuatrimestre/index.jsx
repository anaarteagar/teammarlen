import React, { useState, useEffect } from 'react';
import NavBar from '../NavBar';
import { PlusOutlined, EditOutlined, DeleteOutlined, LinkOutlined, UserOutlined } from '@ant-design/icons';
import { Space, Table, Button } from 'antd';
import { useAuth } from '../../hooks/useAuth';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';
import { cuatrimestreServices } from '../../services/cuatrimestre';
import ModalAdd from '../ModalsCuatrimestre/ModalAdd';
import ModalEdit from '../ModalsCuatrimestre/ModalEdit';
import ModalDelete from '../ModalsCuatrimestre/ModalDelete';

const TableCuatrimestre = () => {

    const { user } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const adminRoleId = '66789c052ed810254bf43470';
    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);

    const [selectedCuatrimestre, setSelectedCuatrimestre] = useState(null);

    useEffect(() => {
        document.title = 'Cuatrimestre';
        changeFavicon(InicioFavicon);
    }, []);

    const showModalEdit = (cuatrimestre) => {
        setSelectedCuatrimestre(cuatrimestre);
        setIsModalEditOpen(true);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
        setSelectedCuatrimestre(null);
    };

    const showModalDelete = (cuatrimestre) => {
        setSelectedCuatrimestre(cuatrimestre);
        setIsModalDeleteOpen(true);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
        setSelectedCuatrimestre(null);
    };

    //Inicio de Modal de agregar de materia
    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const fetchCua = async () => {
        try {
            const token = localStorage.getItem('token');
            const usersData = await cuatrimestreServices.getCuatrimestre(token);
            const formattedData = usersData.map((item, index) => ({
                ...item,
                key: index + 1,
            }));
            console.log(formattedData);
            setData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener los cuatrimestres:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchCua();
    }, []);

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            key: '_id',
            align: 'left',
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            align: 'left',
        },
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                <Space>
                    <Button className="edit-button" onClick={() => showModalEdit(record)}>
                        <EditOutlined />
                    </Button>
                    <Button className="delete-button" onClick={() => showModalDelete(record)}>
                        <DeleteOutlined />
                    </Button>
                </Space>
            )
        });
    }


    return (
        <>
            <NavBar />
            <div className='container'>
                <div className='table-header'>
                    <div className='table-header-title'>
                        <h3>Cuatrimestres</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Button className="add-button2" onClick={showModalAdd}>
                                    <PlusOutlined /> Agregar
                                </Button>
                            </Space>
                        )}
                    </div>
                    <Table
                        columns={columns}
                        dataSource={data}
                        loading={loading}
                        pagination={{ pageSize: 5 }}
                    />
                </div>
                <ModalAdd
                    isVisible={isModalAddOpen}
                    onCancel={handleCancelAdd}
                    loading={loading}
                    setLoading={setLoading}
                    fetchData={fetchCua}
                />
                <ModalEdit
                    isVisible={isModalEditOpen}
                    onCancel={handleCancelEdit}
                    loading={loading}
                    cuatrimestre={selectedCuatrimestre}
                    setLoading={setLoading}
                    fetchData={fetchCua}
                />
                <ModalDelete
                    isVisible={isModalDeleteOpen}
                    onCancel={handleCancelDelete}
                    loading={loading}
                    cuatrimestre={selectedCuatrimestre}
                    setLoading={setLoading}
                    fetchData={fetchCua}
                />
            </div>
        </>

    );
}

export default TableCuatrimestre;