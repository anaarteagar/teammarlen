import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, notification } from 'antd';
import { cuatrimestreServices } from '../../services/cuatrimestre';
import './CuatrimestreModals.css';

const ModalEdit = ({ isVisible, onCancel, loading, setLoading, fetchData, cuatrimestre }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (cuatrimestre) {
            form.setFieldsValue({
                nombre: cuatrimestre.nombre,
            });
        }
    }, [cuatrimestre, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem("token");
            await cuatrimestreServices.editCuatrimestre(token, cuatrimestre._id, values);
            notification.success({ message: 'Cuatrimestre actualizado correctamente'});
            fetchData();
            onCancel();
        } catch (error) {
            console.error("Error al actualizar el cuatrimestre: ", error);
            notification.error({ message: "Error al actualizar el cuatrimestre" });
        } finally {
            setLoading(false);
        }
    }

    return ( 
        <Modal
            title="Editar materia"
            visible={isVisible}
            onCancel={onCancel}
            footer={null}
        >
            <Form
                form={form}
                name="edit_materia"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Materia"
                    rules={[{ required: true, message: "Por favor no borres los datos de este campo" }]}
                >
                    <Input placeholder='Nombre de la materia' className="modal-input" />
                </Form.Item>
                <Form.Item className="modal-buttons">
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginRight: '16px' }}>
                        Editar cuatrimestre
                    </Button>
                    <Button onClick={onCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalEdit;