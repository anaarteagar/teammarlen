import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, notification } from 'antd';
import { cuatrimestreServices } from '../../services/cuatrimestre';
import './CuatrimestreModals.css';

const ModalAdd = ({ isVisible, onCancel, loading, setLoading, fetchData }) => {
    const [form] = Form.useForm();

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    useEffect(() => {
        if (isVisible) {
            form.resetFields();
        }
    }, [isVisible, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await cuatrimestreServices.addCuatrimestre(token,values);
            notification.success({ message: "Cuatrimestre creada con éxito" });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al crear el cuatrimestre: ', error);
            notification.error({ message: "Error al crear el cuatrimestre" });
        } finally {
            setLoading(false);
        }
    }

    return (
        <Modal
            title="Agregar nuevo Cuatrimestre"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
        >
            <Form
                form={form}
                name="add_cuatrimestre"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Cuatrimestre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre del cuatrimestre' }]}
                >
                    <Input placeholder='Nombre del cuatrimestre' className="modal-input" />
                </Form.Item>
                <Form.Item className="modal-buttons">
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginRight: '16px' }}>
                        Crear Cuatrimestre
                    </Button>
                    <Button onClick={handleCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAdd;