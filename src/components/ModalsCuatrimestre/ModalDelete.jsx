import React from 'react';
import { Modal, Button, notification } from 'antd';
import { cuatrimestreServices } from '../../services/cuatrimestre';
import { DeleteOutlined } from '@ant-design/icons';
import './CuatrimestreModals.css';

const ModalDelete = ({ isVisible, onCancel, loading, setLoading, fetchData, cuatrimestre }) => {

    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleDelete = async () => {
        setLoading(true);
        try {
            const token = localStorage.getItem("token");
            await cuatrimestreServices.deleteCuatrimestre(token, cuatrimestre._id);
            onCancel();
            fetchData();
            openNotificationWithIcon('success', 'Cuatrimestre eliminado exitosamente', <DeleteOutlined style={{ color: 'red' }} />);
        } catch (error) {
            console.error('Error al eliminar el cuatrimestre:', error);
            openNotificationWithIcon('error', 'Error al eliminar el cuatrimestre', null);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title={<span>Eliminar cuatrimestre</span>}
            visible={isVisible}
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button
                    key="delete"
                    type="primary"
                    loading={loading}
                    onClick={handleDelete}
                    className="modal-button submit-button"
                >
                    Eliminar
                </Button>,
            ]}
        >
            <p>¿Está seguro de que desea eliminar al "<strong>{cuatrimestre && cuatrimestre.nombre}</strong>"?</p>
        </Modal>
    );
};

export default ModalDelete;
