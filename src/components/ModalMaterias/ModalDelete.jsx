import React from 'react';
import { Modal, Button, notification } from 'antd';
import { materiaServices } from '../../services/materia';
import { DeleteOutlined } from '@ant-design/icons';

const ModalDelete = ({ isVisible, onCancel, materia, loading, setLoading, fetchData }) => {
    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleDelete = async () => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await materiaServices.deleteMateria(token, materia._id);
            fetchData();
            onCancel();
            openNotificationWithIcon('success', 'Materia eliminada exitosamente', <DeleteOutlined style={{ color: 'red' }} />);
        } catch (error) {
            console.error('Error al eliminar la materia:', error);
            openNotificationWithIcon('error', 'Error al eliminar la materia', null);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title={<span>Eliminar materia</span>}
            visible={isVisible}
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button
                    key="delete"
                    type="primary"
                    loading={loading}
                    onClick={handleDelete}
                    className="modal-button submit-button"
                >
                    Eliminar
                </Button>,
            ]}
        >
            <p>¿Está seguro de que desea eliminar la materia <strong>{materia && materia.nombre}</strong>?</p>
        </Modal>
    );
};

export default ModalDelete;
