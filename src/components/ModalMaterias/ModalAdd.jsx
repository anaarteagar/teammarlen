import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, notification } from 'antd';
import { materiaServices } from '../../services/materia';
import './MateriaModals.css';

const ModalAdd = ({ isVisible, onCancel, loading, setLoading, fetchData }) => {
    const [form] = Form.useForm();

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    useEffect(() => {
        if (isVisible) {
            form.resetFields();
        }
    }, [isVisible, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await materiaServices.addMateria(token, values);
            notification.success({ message: "Materia creada con éxito" });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al crear la materia: ', error);
            notification.error({ message: "Error al crear la materia" });
        } finally {
            setLoading(false);
        }
    }
    return (
        <Modal
            title="Agregar nueva Materia"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
        >
            <Form
                form={form}
                name="add_materia"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Materia"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre de la materia' }]}
                >
                    <Input placeholder='Nombre de la materia' className="modal-input" />
                </Form.Item>
                <Form.Item
                    name="descripcion"
                    label="Descripción"
                    rules={[{ required: true, message: 'Por favor ingrese una descripción de la materia' }]}
                >
                    <Input.TextArea rows={4} placeholder='Descripción de la materia' className="modal-textarea" />
                </Form.Item>
                <Form.Item className="modal-buttons">
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginRight: '16px' }}>
                        Crear materia
                    </Button>
                    <Button onClick={handleCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAdd;
