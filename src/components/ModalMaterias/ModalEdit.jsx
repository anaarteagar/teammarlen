import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, notification } from 'antd';
import { materiaServices } from '../../services/materia';
import './MateriaModals.css';

const ModalEdit = ({ isVisible, onCancel, materia, loading, setLoading, fetchData }) => {

    const [form] = Form.useForm();

    useEffect(() => {
        if (materia) {
            form.setFieldsValue({
                nombre: materia.nombre,
                descripcion: materia.descripcion,
            });
        }
    }, [materia, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem("token");

            await materiaServices.editMateria(token, materia._id, values);
            notification.success({ message: 'Materia actualizada correctamente' });
            fetchData();
            onCancel();

        } catch (error) {
            console.error("Error al actualizar la materia: ", error);
            notification.error({ message: "Error al actualizar la materia" });
        } finally {
            setLoading(false);
        }
    }

    return (
        <Modal
            title="Editar materia"
            visible={isVisible}
            onCancel={onCancel}
            footer={null}
        >
            <Form
                form={form}
                name="edit_materia"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Materia"
                    rules={[{ required: true, message: "Por favor no borres los datos de este campo" }]}
                >
                    <Input placeholder='Nombre de la materia' className="modal-input" />
                </Form.Item>
                <Form.Item
                    name="descripcion"
                    label="Descripción"
                    rules={[{ required: true, message: "La descripción es obligatoria" }]}
                >
                    <Input.TextArea rows={4} placeholder='Descripción de la materia' className="modal-textarea" />
                </Form.Item>
                <Form.Item className="modal-buttons">
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginRight: '16px' }}>
                        Editar materia
                    </Button>
                    <Button onClick={onCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalEdit;
