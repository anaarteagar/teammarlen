import React from 'react';
import { Modal, Form, Input, Button, message } from 'antd';
import { sendEmail } from '../../services/correo.js'; 

const ContactModal = ({ isVisible, onCancel, onSubmit }) => {
    const [form] = Form.useForm();

    const handleSubmit = async () => {
        try {
            const values = await form.validateFields();
            const { name, email, asunto, mensaje } = values;

            await sendEmail(name, email, asunto, mensaje);

            message.success('Correo enviado exitosamente');
            form.resetFields();
            onSubmit(); // Cierra el modal después del envío
        } catch (error) {
            message.error(`Error: ${error.message}`);
        }
    };

    const handleModalCancel = () => {
        form.resetFields();
        onCancel(); 
    };

    return (
        <Modal
            title="¿Tienes una pregunta? Envíala al correo"
            visible={isVisible}
            onCancel={handleModalCancel}
            footer={[
                <Button key="cancel" onClick={handleModalCancel} className="modal-button cancel-button">
                    Cancelar
                </Button>,
                <Button key="submit" type="primary" onClick={handleSubmit} className="modal-button submit-button">
                    Enviar
                </Button>
            ]}
        >
            <Form
                form={form}
                name="contact_form"
                layout="vertical"
            >
                <Form.Item
                    label="Nombre"
                    name="name"
                    rules={[{ required: true, message: 'Por favor, ingrese su nombre' }]}
                >
                    <Input placeholder="Ingresa tu nombre" />
                </Form.Item>
                <Form.Item
                    label="Correo electrónico"
                    name="email"
                    rules={[
                        { required: true, message: 'Por favor, ingrese un correo electrónico' },
                        { type: 'email', message: 'Por favor, ingrese un correo electrónico válido' }
                    ]}
                >
                    <Input placeholder="Ingresa tu correo electrónico" />
                </Form.Item>
                <Form.Item
                    label="Asunto"
                    name="asunto"
                    rules={[{ required: true, message: 'Por favor, ingrese el asunto de su mensaje' }]}
                >
                    <Input.TextArea rows={1} placeholder="Escribe el asunto aquí" />
                </Form.Item>
                <Form.Item
                    label="Mensaje"
                    name="mensaje"
                    rules={[{ required: true, message: 'Por favor, ingrese su mensaje' }]}
                >
                    <Input.TextArea rows={4} placeholder="Escribe tu mensaje aquí" />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ContactModal;
