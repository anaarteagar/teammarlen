import React, { useState } from 'react';
import './NavBar.css';
import { useAuth } from '../../hooks/useAuth';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faSignOutAlt, faUserTie, faBook } from '@fortawesome/free-solid-svg-icons';
import { BookOutlined, BookFilled, ScheduleOutlined } from '@ant-design/icons';
import SideMenu from '../SideMenu';
import logo from '../../assets/img/b7.png';

const NavBar = () => {
    const { user, logout } = useAuth();
    const [showMenu, setShowMenu] = useState(false);
    const navigate = useNavigate();
    const adminRoleId = '66789c052ed810254bf43470';
    const customerRoleId = '66789c052ed810254bf4346f';

    const handleLogout = () => {
        logout();
    };

    const handleLogin = () => {
        navigate('/login');
    };
    const handleAdmisiones = () => {
        navigate('/admisiones');
    };
    const handleUsers = () => {
        navigate('/usuarios');
    };
    const handleTeachers = () => {
        navigate('/profesores');
    };
    const handleOferta = () => {
        navigate('/oferta');
    };
    const handleNateria = () => {
        navigate('/materias');
    };
    const handleCuatrimestre = () => {
        navigate('/cuatrimestre');
    };
    const handleHome = () => {
        navigate('/');
    };

    const toggleMenu = () => {
        setShowMenu(!showMenu);
    };

    return (
        <nav className="navbar">
            <div className="navbar-brand" onClick={handleHome}>
                <img src={logo} alt="Logo" className="navbar-logo" />
                <span className="navbar-title">LIZARD 2.0</span>
            </div>
            <ul className="navbar-menu">
                {user && user.roles && (user.roles.includes(adminRoleId) || user.roles.includes(customerRoleId)) ? (
                    <>
                        <li className="navbar-item">
                            <a href="" onClick={handleAdmisiones}>
                                <FontAwesomeIcon icon={faUserTie} /> Admisiones
                            </a>
                        </li>
                        {user && user.roles && (user.roles.includes(adminRoleId)) ? (
                            <li className="navbar-item">
                                <a href="" onClick={handleUsers}>
                                    <FontAwesomeIcon icon={faUserTie} /> Usuarios
                                </a>
                            </li>
                        ) : null}

                        <li className="navbar-item">
                            <a href="" onClick={handleTeachers}>
                                <FontAwesomeIcon icon={faUserTie} /> Profesores
                            </a>
                        </li>
                        <li className="navbar-item">
                            <a href="" onClick={handleOferta}>
                                <BookOutlined icon={faUserTie} /> Oferta Educativa
                            </a>
                        </li>
                        <li className="navbar-item">
                            <a href="" onClick={handleNateria}>
                            <FontAwesomeIcon icon={faBook} /> Materia
                            </a>
                        </li>
                        <li className="navbar-item">
                            <a href="" onClick={handleCuatrimestre}>
                            <ScheduleOutlined /> Cuatrimestre
                            </a>
                        </li>
                        <li className="navbar-item">
                            <a href="#perfil" onClick={toggleMenu}>
                                <FontAwesomeIcon icon={faUser} /> Perfil
                            </a>
                        </li>
                    </>
                ) : (
                    <li className="navbar-item">
                        <button onClick={handleLogin}>
                            <FontAwesomeIcon icon={faUser} /> Iniciar sesión
                        </button>
                    </li>
                )}
            </ul>
            {showMenu && <SideMenu onClose={toggleMenu} />}
        </nav>
    );
};

export default NavBar;
