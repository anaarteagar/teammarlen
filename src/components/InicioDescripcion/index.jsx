import React from 'react';
import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';
import './InicioDescripcion.css';

const ComponenteDescripcion = () => {
    const navigate = useNavigate();

    const handleLogin = () => {
        navigate('/login');
    };

    return (
        <div className="descripcion-container">
            <h2>BIENVENIDO A LIZARD 2.0</h2>
            <h3 className='texto-negro'>Esta aplicación te permite gestionar los datos de la universidad, sus ofertas
            educativas, admisiones, divisiones y profesores de manera eficiente.</h3>
            <div className="image-content">
                <img src="https://www.liderempresarial.com/wp-content/uploads/2021/12/UTEQ-diplomado-Industria-4.0-ch.jpg" alt="UTEQ" />
            </div>
        </div>
    );
};

export default ComponenteDescripcion;
