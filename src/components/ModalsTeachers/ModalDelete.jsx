import React from 'react';
import { Modal, Button, notification } from 'antd';
import { profesoresService } from '../../services/profesores';
import { DeleteOutlined } from '@ant-design/icons';

const ModalDelete = ({ isVisible, onCancel, user, loading, setLoading, fetchData }) => {
    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleDelete = async () => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await profesoresService.deleteProfesor(token, user._id);
            fetchData();
            onCancel();
            openNotificationWithIcon('success', 'Profesor eliminado exitosamente', <DeleteOutlined style={{ color: 'red' }} />);
        } catch (error) {
            console.error('Error al eliminar el profesor:', error);
            openNotificationWithIcon('error', 'Error al eliminar el profesor', <DeleteOutlined style={{ color: 'red' }} />);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Eliminar Profesor"
            visible={isVisible}
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button key="delete"  type="primary" loading={loading} onClick={handleDelete} className="modal-button submit-button">
                    Eliminar
                </Button>,
            ]}
        >
            <p>¿Está seguro de que desea eliminar al profesor {user && user.nombre} {user && user.apellidos} con correo {user && user.correo}?</p>
        </Modal>
    );
};

export default ModalDelete;