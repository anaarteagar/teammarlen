import { Modal, Input, Form, notification, DatePicker, Button } from 'antd';
import { useEffect } from 'react';
import { profesoresService } from '../../services/profesores';
import moment from 'moment';
import './ProfesoresModals.css';

const ModalEdit = ({ isVisible, onCancel, loading, setLoading, user, fetchData }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (user) {
            form.setFieldsValue({
                nombre: user.nombre,
                apellidos: user.apellidos,
                numeroEmpleado: user.numeroEmpleado,
                correo: user.correo,
                fechaNacimiento: user.fechaNacimiento ? moment(user.fechaNacimiento) : null,
            });
        }
    }, [user, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');

            // Preparar los datos actualizados del profesor
            const profesorData = {
                nombre: values.nombre,
                apellidos: values.apellidos,
                numeroEmpleado: values.numeroEmpleado,
                correo: values.correo,
                fechaNacimiento: values.fechaNacimiento ? values.fechaNacimiento.format('YYYY-MM-DD') : null,
            };

            // Llamar al servicio para actualizar el profesor
            await profesoresService.updateProfesor(token, user._id, profesorData);

            notification.success({ message: 'Profesor actualizado correctamente' });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al actualizar profesor:', error);
            notification.error({ message: 'Error al actualizar el profesor' });
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Editar profesor"
            visible={isVisible}
            onCancel={onCancel}
            footer={null} // Evita el uso del pie de modal predeterminado
        >
            <Form
                form={form}
                name="edit_profesor"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="apellidos"
                    label="Apellidos"
                    rules={[{ required: true, message: 'Por favor ingrese los apellidos' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="numeroEmpleado"
                    label="No. empleado"
                    rules={[{ required: true, message: 'Por favor ingrese el número de empleado' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="correo"
                    label="Correo"
                    rules={[
                        { required: true, message: 'Por favor ingrese el correo' },
                        { type: 'email', message: 'Por favor ingrese un correo válido' }
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="fechaNacimiento"
                    label="Fecha de Nacimiento"
                    rules={[{ required: true, message: 'Por favor seleccione la fecha de nacimiento' }]}
                    className="date-picker-item"
                >
                    <DatePicker format="YYYY-MM-DD" />
                </Form.Item>
                <Form.Item wrapperCol={{ span: 24 }} className="button-group">
                    <Button onClick={onCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginLeft: '16px' }}>
                        Guardar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalEdit;
