import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, DatePicker, notification } from 'antd';
import { profesoresService } from '../../services/profesores';
import './ProfesoresModals.css';

const ModalAdd = ({ isVisible, onCancel, loading, setLoading, fetchData }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (isVisible) {
            form.resetFields();
        }
    }, [isVisible, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');

            const profesor = {
                nombre: values.nombre,
                apellidos: values.apellidos,
                numeroEmpleado: values.numeroEmpleado,
                correo: values.correo,
                fechaNacimiento: values.fechaNacimiento ? values.fechaNacimiento.format('YYYY-MM-DD') : null,
            }

            // Llamada API para agregar el usuario
            await profesoresService.addProfesor(token, profesor);
            notification.success({ message: 'Usuario creado con éxito', placement: 'bottomRight' });
            fetchData(); // Invoca fetchUsers para actualizar la lista
            onCancel();
            console.log("Fecha:\n",profesor.fechaNacimiento);
        } catch (error) {
            console.error('Error al crear usuario:', error);
            notification.error({ message: 'Error al crear usuario', placement: 'bottomRight' });
        } finally {
            setLoading(false);
        }
    };

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    return (
        <Modal
            title="Agregar Nuevo Profesor"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
        >
            <Form
                form={form}
                name="add_profesor"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Nombre"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="apellidos"
                    label="Apellidos"
                    rules={[{ required: true, message: 'Por favor ingrese los apellidos' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="numeroEmpleado"
                    label="No. empleado"
                    rules={[{ required: true, message: 'Por favor ingrese el número de empleado' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="correo"
                    label="Correo"
                    rules={[
                        { required: true, message: 'Por favor ingrese el correo' },
                        { type: 'email', message: 'Por favor ingrese un correo válido' }
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="fechaNacimiento"
                    label="Fecha Nacimiento"
                    rules={[{ required: true, message: 'Por favor seleccione la fecha de nacimiento' }]}
                >
                    <DatePicker format="YYYY-MM-DD" placeholder="YYYY-MM-DD" />
                </Form.Item>
                <Form.Item wrapperCol={{ span: 24 }} className="button-group">
                    <Button onClick={handleCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginLeft: '16px' }}>
                        Crear Usuario
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalAdd;
