import React from 'react';
import { Modal, Table } from 'antd';

const ModalTeachers = ({ isVisible, onCancel, oferta }) => {
    console.log('Oferta recibida en ModalTeachers:', oferta);

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
        },
        {
            title: 'Apellidos',
            dataIndex: 'apellidos',
            key: 'apellidos',
        },
    ];

    return (
        <Modal
            title={`Maestros de ${oferta?.nombre}`}
            visible={isVisible}
            onCancel={onCancel}
            footer={null}
            width={800}
        >
            <Table
                columns={columns}
                dataSource={oferta?.profesores}
                rowKey={(record) => `${record.nombre}-${record.apellidos}`}
                pagination={{ pageSize: 5 }}
                locale={{ emptyText: 'No hay profesores asociados' }}
            />
        </Modal>
    );
};

export default ModalTeachers;