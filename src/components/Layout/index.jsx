import React from 'react';
import { Col, Row } from 'antd';
import FormLogin from '../FormLogin';
import "./Layout.css"

const LayoutComponent = ({ leftColSize, rightColSize, leftContent, rightContent }) => {
    return (
        <div className="layout-container">
            <Row>
                <Col xs={0} sm={0} md={0} lg={0}>
                    <div className="content-left">      
                        {leftContent}
                    </div>
                </Col>
                <Col xs={24} sm={24} md={24} lg={24}>
                <div className="content-right">
                        {rightContent}
                    </div>
                </Col>
            </Row>
        </div>
    );
}

export default LayoutComponent;
