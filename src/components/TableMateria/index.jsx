import React, { useState, useEffect } from 'react';
import NavBar from '../NavBar';
import { Space, Table, Button, Input } from 'antd';
import { PlusOutlined, EditOutlined, DeleteOutlined, FilePdfOutlined } from '@ant-design/icons';
import { materiaServices } from '../../services/materia';
import { useAuth } from '../../hooks/useAuth';
import ModalAdd from '../ModalMaterias/ModalAdd';
import ModalDelete from '../ModalMaterias/ModalDelete';
import ModalEdit from '../ModalMaterias/ModalEdit';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';
import generateMateriasPDF from '../../services/generateMateriasPDF';

const TableMateria = () => {
    useEffect(() => {
        document.title = 'Materias';
        changeFavicon(InicioFavicon);
    }, []);

    const { user } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const [hasSearchResults, setHasSearchResults] = useState(true);
    const adminRoleId = '66789c052ed810254bf43470';

    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);

    const [selectedMateria, setSelectedMateria] = useState(null);

    const fetchMat = async () => {
        try {
            const token = localStorage.getItem('token');
            const usersData = await materiaServices.materias(token);
            const formattedData = usersData.map((item, index) => ({
                ...item,
                key: index + 1,
            }));
            console.log(formattedData);
            setData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener las materias:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchMat();
    }, []);

    const showModalEdit = (materia) => {
        setSelectedMateria(materia);
        setIsModalEditOpen(true);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
        setSelectedMateria(null);
    };

    const showModalDelete = (materia) => {
        setSelectedMateria(materia);
        setIsModalDeleteOpen(true);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
        setSelectedMateria(null);
    };

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const handleSearch = (e) => {
        setSearchText(e.target.value);
    };

    const handleGeneratePDF = () => {
        generateMateriasPDF(filteredData, user.username);
    };

    const filteredData = data.filter(materia => {
        const searchTextLower = searchText.toLowerCase();
        return materia.nombre.toLowerCase().includes(searchTextLower);
    });

    useEffect(() => {
        setHasSearchResults(filteredData.length > 0);
    }, [filteredData]);

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            key: '_id',
            align: 'left',
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            align: 'left',
        },
        {
            title: 'Descripción',
            dataIndex: 'descripcion',
            key: 'descripcion',
            align: 'left',
            render: (text) => (
                <div style={{ maxHeight: '100px', overflow: 'auto' }}>
                    {text}
                </div>
            ),
        },        
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                <Space>
                    <Button className="edit-button" onClick={() => showModalEdit(record)}>
                        <EditOutlined />
                    </Button>
                    <Button className="delete-button" onClick={() => showModalDelete(record)}>
                        <DeleteOutlined />
                    </Button>
                </Space>
            )
        });
    }

    const locale = {
        emptyText: hasSearchResults ? 'Cargando' : 'No se han encontrado coincidencias',
    };

    return (
        <>
            <NavBar />
            <div className='container'>
                <div className='table-header'>
                    <div className='table-header-title'>
                        <h3>Materias</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Input 
                                    placeholder="Buscar materias" 
                                    value={searchText} 
                                    onChange={handleSearch} 
                                    style={{ width: 200, marginRight: 10 }} 
                                />
                                <Button className="add-button2" onClick={showModalAdd}>
                                    <PlusOutlined /> Agregar
                                </Button>
                                <Button onClick={handleGeneratePDF}>
                                    <FilePdfOutlined /> Generar PDF
                                </Button>
                            </Space>
                        )}
                    </div>
                    <Table
                        columns={columns}
                        dataSource={filteredData}
                        loading={loading}
                        pagination={{ pageSize: 5 }}
                        locale={locale}
                    />
                </div>
                <ModalAdd
                    isVisible={isModalAddOpen}
                    onCancel={handleCancelAdd}
                    loading={loading}
                    setLoading={setLoading}
                    fetchData={fetchMat}
                />
                <ModalDelete
                    isVisible={isModalDeleteOpen}
                    onCancel={handleCancelDelete}
                    loading={loading}
                    materia={selectedMateria}
                    setLoading={setLoading}
                    fetchData={fetchMat}
                />
                <ModalEdit
                    isVisible={isModalEditOpen}
                    onCancel={handleCancelEdit}
                    loading={loading}
                    materia={selectedMateria}
                    setLoading={setLoading}
                    fetchData={fetchMat}
                />
            </div>
        </>
    );
}

export default TableMateria;
