import React, { useState, useEffect } from 'react';
import NavBar from '../NavBar';
import { Space, Table, Tag, Button, Input } from 'antd';
import { PlusOutlined, EditOutlined, DeleteOutlined, LinkOutlined, UserOutlined, FilePdfOutlined } from '@ant-design/icons';
import { useAuth } from '../../hooks/useAuth';
import { ofertaService } from '../../services/ofertaEducativa';
import ModalAdd from '../ModalsOferta/ModalAdd';
import ModalDelete from '../ModalsOferta/ModalDelete';
import ModalEdit from '../ModalsOferta/ModalEdit';
import ModalLinkAdmisiones from '../ModalsOferta/ModalLinkAdmisiones';
import ModalTeachers from '../ModalsTeachers/ModalTeachers';
import generateOfertasPDF from '../../services/generateOfertasPDF';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';

const Tableoferta = () => {
    const { user, logout } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const [hasSearchResults, setHasSearchResults] = useState(true);

    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
    const [isModalLinkAdmisionesOpen, setIsModalLinkAdmisionesOpen] = useState(false);
    const [isModalTeachersOpen, setIsModalTeachersOpen] = useState(false);
    const adminRoleId = '66789c052ed810254bf43470';

    const [selectedOfert, setSelectedOfert] = useState(null);

    useEffect(() => {
        document.title = 'Oferta Educativa';
        changeFavicon(InicioFavicon);
    }, []);

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const showModalEdit = (user) => {
        setSelectedOfert(user);
        setIsModalEditOpen(true);
    };

    const showModalDelete = (user) => {
        setSelectedOfert(user);
        setIsModalDeleteOpen(true);
    }

    const showModalLinkAdmisiones = (user) => {
        setSelectedOfert(user);
        setIsModalLinkAdmisionesOpen(true);
    }

    const showModalTeachers = (user) => {
        setSelectedOfert(user);
        setIsModalTeachersOpen(true);
    }

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
        setSelectedOfert(null);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
        setSelectedOfert(null);
    };

    const handleCancelLinkAdmisiones = () => {
        setIsModalLinkAdmisionesOpen(false);
        setSelectedOfert(null);
    };

    const handleCancelTeachers = () => {
        setIsModalTeachersOpen(false);
        setSelectedOfert(null);
    };

    const fetchOfert = async () => {
        try {
            const token = localStorage.getItem('token');
            const usersData = await ofertaService.getOferta(token);
            const formattedData = usersData.map((item, index) => ({
                ...item,
                key: index + 1,
            }));
            setData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener las ofertas:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchOfert();
    }, []);

    const handleGeneratePDF = () => {
        generateOfertasPDF(filteredData, user.username);
    };

    const handleSearch = (e) => {
        setSearchText(e.target.value);
    };

    const filteredData = data.filter(oferta => {
        const searchTextLower = searchText.toLowerCase();
        const isNameMatch = oferta.nombre.toLowerCase().includes(searchTextLower);
        const isEstadoMatch = searchTextLower === 'activo' && oferta.activo ||
                             searchTextLower === 'inactivo' && !oferta.activo;
        const isAdmisionesMatch = oferta.admisiones.toString().toLowerCase().includes(searchTextLower);
        return isNameMatch || isEstadoMatch || isAdmisionesMatch;
    });

    useEffect(() => {
        setHasSearchResults(filteredData.length > 0);
    }, [filteredData]);

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            key: '_id',
            align: 'left',
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            align: 'left',
        },
        {
            title: 'Estado',
            dataIndex: 'activo',
            key: 'activo',
            align: 'left',
            render: (activo) => (
                <Tag color={activo ? 'green' : 'red'}>
                    {activo ? 'Activo' : 'Inactivo'}
                </Tag>
            ),
        },
        {
            title: 'Admisiones',
            dataIndex: 'admisiones',
            key: 'admisiones',
            align: 'left',
        },
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                <Space>
                    <Button className="edit-button" onClick={() => showModalEdit(record)}>
                        <EditOutlined />
                    </Button>
                    <Button className="delete-button" onClick={() => showModalDelete(record)}>
                        <DeleteOutlined />
                    </Button>
                    {/* <Button
                        className="link-button"
                        onClick={() => showModalLinkAdmisiones(record)}
                        disabled={record.admisiones.length > 0}
                    >
                        <LinkOutlined />
                    </Button> */}
                    <Button 
                        className="teachers-button" 
                        onClick={() => showModalTeachers(record)}
                    >
                        <UserOutlined />
                    </Button>
                </Space>
            )
        });
    }

    const locale = {
        emptyText: hasSearchResults ? 'Cargando' : 'No se han encontrado coincidencias',
    };

    return (
        <>
            <NavBar />
            <div className='container'>
                <div className="table-header">
                    <div className="table-header-title">
                        <h3>Ofertas Educativas</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Input 
                                    placeholder="Buscar ofertas" 
                                    value={searchText} 
                                    onChange={handleSearch} 
                                    style={{ width: 200, marginRight: 10 }} 
                                />
                                <Button className="add-button2" onClick={showModalAdd}>
                                    <PlusOutlined /> Agregar
                                </Button>
                                <Button onClick={handleGeneratePDF}>
                                    <FilePdfOutlined /> Generar PDF
                                </Button>
                            </Space>
                        )}
                    </div>
                    <Table
                        columns={columns}
                        dataSource={filteredData}
                        loading={loading}
                        pagination={{ pageSize: 6 }}
                        locale={locale}
                        scroll={{ y: 260 }}
                    />
                </div>
                <ModalAdd
                    isVisible={isModalAddOpen}
                    onCancel={handleCancelAdd}
                    loading={loading}
                    setLoading={setLoading}
                    fetchData={fetchOfert}
                />
                <ModalEdit
                    isVisible={isModalEditOpen}
                    onCancel={handleCancelEdit}
                    loading={loading}
                    setLoading={setLoading}
                    oferta={selectedOfert}
                    fetchData={fetchOfert}
                />
                <ModalDelete
                    isVisible={isModalDeleteOpen}
                    onCancel={handleCancelDelete}
                    loading={loading}
                    setLoading={setLoading}
                    oferta={selectedOfert}
                    fetchData={fetchOfert}
                />
                <ModalLinkAdmisiones
                    isVisible={isModalLinkAdmisionesOpen}
                    onCancel={handleCancelLinkAdmisiones}
                    loading={loading}
                    setLoading={setLoading}
                    oferta={selectedOfert}
                    fetchData={fetchOfert}
                />
                <ModalTeachers
                    isVisible={isModalTeachersOpen}
                    onCancel={handleCancelTeachers}
                    oferta={selectedOfert}
                />
            </div>
        </>
    )
}

export default Tableoferta;
