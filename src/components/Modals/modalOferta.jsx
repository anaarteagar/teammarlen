import React from 'react';
import { Modal, Descriptions, Tag } from 'antd';

const ModalOferta = ({ visible, onClose, ofertas }) => {
    return (
        <Modal
            visible={visible}
            title="Detalles de las Ofertas Educativas"
            onCancel={onClose}
            footer={null}
        >
            {ofertas.length > 0 ? (
                ofertas.map((oferta, index) => (
                    <Descriptions bordered key={index} style={{ marginBottom: '20px' }}>
                        <Descriptions.Item label="Nombre">{oferta.nombre}</Descriptions.Item>
                        <Descriptions.Item label="Activo">
                            <Tag color={oferta.activo ? 'green' : 'red'}>
                                {oferta.activo ? 'Activa' : 'Inactiva'}
                            </Tag>
                        </Descriptions.Item>
                    </Descriptions>
                ))
            ) : (
                <p>No hay detalles disponibles para estas ofertas educativas.</p>
            )}
        </Modal>
    );
};

export default ModalOferta;
