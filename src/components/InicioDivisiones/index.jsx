import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import { FilePdfOutlined } from '@ant-design/icons';
import { divisionesService } from '../../services/divisiones';
import generateOfertaMateriasPDF from '../../services/generateOfertaMateriasPDF'; 
import './InicioDivisiones.css';

const ComponenteDivisiones = () => {
    const [divisiones, setDivisiones] = useState([]);

    const handleGeneratePDF = (oferta) => {
        generateOfertaMateriasPDF(oferta);
    };

    useEffect(() => {
        const fetchDivisiones = async () => {
            try {
                const data = await divisionesService.divisiones();
                setDivisiones(data);
            } catch (error) {
                console.error('Error fetching divisiones:', error);
            }
        };

        fetchDivisiones();
    }, []);

    return (
        <div className="admisiones-container">
            <h2>Nuestras Divisiones</h2>

            {divisiones.length > 0 ? (
                divisiones.map((division) => (
                    <div key={division._id} className="division-card">
                        <h3 className='texto-negro'>{division.nombre}</h3>
                        <h4 className='texto-negro'>{division.descripcion}</h4>

                        {division.ofertasEducativas.length > 0 ? (
                            <ul>
                                {division.ofertasEducativas.map((oferta) => (
                                    <li key={oferta._id}>
                                        <h4 className='texto-negro'>{oferta.nombre}</h4>

                                        {oferta.materias.length > 0 ? (
                                            <>
                                                <div className="materias-container">
                                                    <h4 className='texto-negro'>
                                                        Materias disponibles: 
                                                        <Button
                                                            className='btn-pdfEP'
                                                            onClick={() => handleGeneratePDF(oferta)}
                                                            style={{ marginLeft: 10 }}
                                                        >
                                                            <FilePdfOutlined /> Generar PDF
                                                        </Button>
                                                    </h4>
                                                    <ul>
                                                        {oferta.materias.map((materia) => (
                                                            <li className='texto-negro' key={materia._id}>
                                                                {materia.nombre}
                                                            </li>
                                                        ))}
                                                    </ul>
                                                </div>
                                            </>
                                        ) : (
                                            <p className='texto-negro'>No hay materias disponibles.</p>
                                        )}
                                    </li>
                                ))}
                            </ul>
                        ) : (
                            <p className='texto-negro'>No hay ofertas educativas disponibles.</p>
                        )}
                    </div>
                ))
            ) : (
                <p>No hay divisiones disponibles.</p>
            )}
        </div>
    );
};

export default ComponenteDivisiones;
