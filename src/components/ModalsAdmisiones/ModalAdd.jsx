import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, Switch, notification, Select, List } from 'antd';
import { PlusOutlined, DeleteOutlined } from '@ant-design/icons';
import { admisionesService } from '../../services/admisones';
import { ofertaService } from '../../services/ofertaEducativa';
import './AdmisionModals.css';

const { Option } = Select;

const ModalAdd = ({ isVisible, onCancel, fetchData }) => {
    const [form] = Form.useForm();
    const [ofertas, setOfertas] = useState([]);
    const [selectedOfertas, setSelectedOfertas] = useState([]);

    const fetchOfertas = async () => {
        try {
            const token = localStorage.getItem('token');
            const ofertasData = await ofertaService.getOferta(token);
            setOfertas(ofertasData);
        } catch (error) {
            console.error('Error al obtener las ofertas:', error);
        }
    };

    useEffect(() => {
        if (isVisible) {
            fetchOfertas();
        }
    }, [isVisible]);

    const handleAddOferta = (value) => {
        const ofertaExistente = selectedOfertas.some(oferta => oferta._id === value);
        if (!ofertaExistente) {
            const ofertaSeleccionada = ofertas.find(oferta => oferta._id === value);
            setSelectedOfertas([...selectedOfertas, ofertaSeleccionada]);
        } else {
            notification.warning({ message: 'La oferta ya está en la lista' });
        }
    };

    const handleRemoveOferta = (id) => {
        setSelectedOfertas(selectedOfertas.filter(oferta => oferta._id !== id));
    };

    const handleOk = async () => {
        try {
            const token = localStorage.getItem('token');
            const values = await form.validateFields();
            const admissionData = { 
                nombre: values.nombre, 
                activo: values.activo, 
            };

            // Crear la nueva admisión
            const newAdmission = await admisionesService.addAdmision(token, admissionData);
            const admisionId = newAdmission._id;

            // Vincular las ofertas educativas con la nueva admisión
            const ofertaIds = selectedOfertas.map(oferta => oferta._id);
            const data = { admisionId, ofertaIds };
            await ofertaService.asigOfertAdmin(token, data);

            notification.success({ message: 'Admisión y ofertas vinculadas exitosamente', icon: <PlusOutlined style={{ color: 'green' }} />, placement: 'bottomRight' });
            form.resetFields();
            setSelectedOfertas([]);
            onCancel();
            fetchData();
        } catch (error) {
            console.error('Error al agregar la admisión y vincular ofertas:', error);
            notification.error({ message: 'Error al agregar la admisión y vincular ofertas', description: error.message });
        }
    };

    return (
        <Modal
            title='Agregar Admisión'
            visible={isVisible}
            onOk={handleOk}
            onCancel={() => {
                onCancel();
                setSelectedOfertas([]);
                form.resetFields();
            }}
            okText='Agregar'
            footer={[
                <Button key="back" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button key="submit" type="primary" onClick={handleOk}>
                    Agregar
                </Button>,
            ]}
        >
            <Form form={form} layout="vertical">
                <Form.Item
                    label="Nombre"
                    name="nombre"
                    rules={[{ required: true, message: 'Por favor ingrese un nombre' }]}
                >
                    <Input className="modal-input" />
                </Form.Item>
                <Form.Item
                    label="Activo"
                    name="activo"
                    valuePropName="checked"
                    initialValue={true}
                >
                    <Switch />
                </Form.Item>
                <Form.Item label="Ofertas Educativas">
                    <Select
                        showSearch
                        placeholder="Selecciona una oferta"
                        optionFilterProp="children"
                        onChange={handleAddOferta}
                        className="modal-input"
                    >
                        {ofertas.map(oferta => (
                            <Option key={oferta._id} value={oferta._id}>
                                {oferta.nombre}
                            </Option>
                        ))}
                    </Select>
                    <List
                        size="small"
                        bordered
                        dataSource={selectedOfertas}
                        renderItem={item => (
                            <List.Item
                                actions={[<Button icon={<DeleteOutlined />} onClick={() => handleRemoveOferta(item._id)} />]}
                            >
                                {item.nombre}
                            </List.Item>
                        )}
                        style={{ marginTop: '10px' }}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalAdd;
