import React, { useEffect, useState } from 'react';
import { Modal, Button, Form, Input, Switch, notification, Select, List } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { admisionesService } from '../../services/admisones';
import { ofertaService } from '../../services/ofertaEducativa';
import './AdmisionModals.css';

const { Option } = Select;

const ModalEdit = ({ admisionId, isVisible, onCancel, fetchData }) => {
    const [form] = Form.useForm();
    const [ofertas, setOfertas] = useState([]);
    const [selectedOfertas, setSelectedOfertas] = useState([]);

    const fetchOfertas = async () => {
        try {
            const token = localStorage.getItem('token');
            const ofertasData = await ofertaService.getOferta(token);
            setOfertas(ofertasData);
        } catch (error) {
            console.error('Error al obtener las ofertas:', error);
        }
    };

    const fetchAdmissionData = async () => {
        try {
            const token = localStorage.getItem('token');
            const admissionData = await admisionesService.getAdmision(token, admisionId);
            form.setFieldsValue({
                nombre: admissionData.nombre,
                activo: admissionData.activo,
            });
            const selectedOfertaData = admissionData.ofertasEducativas.map(ofertaId => 
                ofertas.find(oferta => oferta._id === ofertaId)
            ).filter(Boolean);
            setSelectedOfertas(selectedOfertaData);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        if (isVisible) {
            fetchOfertas();
            fetchAdmissionData();
        }
    }, [isVisible]);

    const handleAddOferta = (value) => {
        const ofertaExistente = selectedOfertas.some(oferta => oferta._id === value);
        if (!ofertaExistente) {
            const ofertaSeleccionada = ofertas.find(oferta => oferta._id === value);
            setSelectedOfertas([...selectedOfertas, ofertaSeleccionada]);
        } else {
            notification.warning({ message: 'La oferta ya está en la lista' });
        }
    };

    const handleRemoveOferta = (id) => {
        setSelectedOfertas(selectedOfertas.filter(oferta => oferta._id !== id));
    };

    const handleOk = async () => {
        try {
            const token = localStorage.getItem('token');
            const values = await form.validateFields();
            const updatedData = { nombre: values.nombre, activo: values.activo };

            // Actualizar la admisión
            await admisionesService.updateAdmision(token, admisionId, updatedData);

            // Vincular las ofertas educativas con la admisión
            const ofertaIds = selectedOfertas.map(oferta => oferta._id);
            const data = { admisionId, ofertaIds };
            await ofertaService.asigOfertAdmin(token, data);

            notification.success({ message: 'Editado exitosamente', icon: <EditOutlined style={{ color: 'blue' }} />, placement: 'bottomRight' });
            form.resetFields();
            setSelectedOfertas([]);
            onCancel();
            fetchData();
        } catch (error) {
            console.error('Error al editar la admisión y vincular ofertas:', error);
            notification.error({ message: 'Error al editar la admisión y vincular ofertas', description: error.message });
        }
    };

    return (
        <Modal
            title='Editar Admisión'
            visible={isVisible}
            onOk={handleOk}
            onCancel={() => {
                onCancel();
                setSelectedOfertas([]);
                form.resetFields();
            }}
            okText='Guardar'
            footer={[
                <Button key="back" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button key="submit" type="primary" onClick={handleOk}>
                    Guardar
                </Button>,
            ]}
        >
            <Form form={form} layout="vertical">
                <Form.Item
                    label="Nombre"
                    name="nombre"
                    rules={[{ required: true, message: 'Por favor ingrese un nombre' }]}
                >
                    <Input className="modal-input" />
                </Form.Item>
                <Form.Item
                    label="Activo"
                    name="activo"
                    valuePropName="checked"
                    initialValue={true}
                >
                    <Switch />
                </Form.Item>
                <Form.Item label="Ofertas Educativas">
                    <Select
                        showSearch
                        placeholder="Selecciona una oferta"
                        optionFilterProp="children"
                        onChange={handleAddOferta}
                        className="modal-input"
                    >
                        {ofertas.map(oferta => (
                            <Option key={oferta._id} value={oferta._id}>
                                {oferta.nombre}
                            </Option>
                        ))}
                    </Select>
                    <List
                        size="small"
                        bordered
                        dataSource={selectedOfertas}
                        renderItem={item => (
                            <List.Item
                                actions={[<Button icon={<DeleteOutlined />} onClick={() => handleRemoveOferta(item._id)} />]}
                            >
                                {item.nombre}
                            </List.Item>
                        )}
                        style={{ marginTop: '10px' }}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalEdit;
