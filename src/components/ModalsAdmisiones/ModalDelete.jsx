import React from 'react';
import { Modal, Button, notification } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { admisionesService } from '../../services/admisones';

const ModalDelete = ({ admisionId, isVisible, onCancel, fetchData }) => {
    const handleOk = async () => {
        try {
            const token = localStorage.getItem('token');
            await admisionesService.deleteAdmissions(token, admisionId);
            notification.success({ message: 'Eliminado exitosamente', icon: <DeleteOutlined style={{ color: 'red' }} /> });
            onCancel();
            fetchData();
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Modal
            title='Eliminar Admisión'
            visible={isVisible}
            onOk={handleOk}
            onCancel={onCancel}
            okText='Eliminar'
        >
            <p>¿Estás seguro que deseas eliminar la admisión?</p>
        </Modal>
    );
};

export default ModalDelete;
