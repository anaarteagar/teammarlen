import React, { useState } from "react";
import { Modal, Button, Form, Input, notification } from "antd";
import { useAuth } from "../../hooks/useAuth";
import { CheckCircleOutlined } from '@ant-design/icons';
import "./FormProfile.css";
import { useNavigate } from 'react-router-dom';
import NavBar from '../NavBar'; 

const FormProfile = () => {
    const navigate = useNavigate();
    const { user, updateUser } = useAuth();
    const [username, setUsername] = useState(user?.username || '');
    const [email, setEmail] = useState(user?.email || '');
    const [imgurl, setImgUrl] = useState(user?.imgurl || '');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [error, setError] = useState('');

    const [isEditProfileModalOpen, setIsEditProfileModalOpen] = useState(false);
    const [isPasswordUpdateModalOpen, setIsPasswordUpdateModalOpen] = useState(false);

    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleEditProfile = () => {
        setIsEditProfileModalOpen(true);
    };

    const handlePasswordUpdate = () => {
        setIsPasswordUpdateModalOpen(true);
    };

    const closeModal = () => {
        setIsEditProfileModalOpen(false);
        setIsPasswordUpdateModalOpen(false);
    };

    const inicio = () => {
        navigate('/');
    };

    const handleSaveChanges = async () => {
        if (password !== confirmPassword) {
            setError('Las contraseñas no coinciden');
            return;
        }

        const userData = {
            id: user._id,
            password
        };

        try {
            await updateUser(userData);
            openNotificationWithIcon('success', 'Contraseña actualizada exitosamente', <CheckCircleOutlined style={{ color: 'green' }} />);
            closeModal();
        } catch (error) {
            console.log("Failed to update password", error);
        }
    };

    const handleSaveProfileChanges = async () => {
        const userData = {
            id: user._id,
            username,
            email,
            imgurl
        };

        try {
            await updateUser(userData);
            openNotificationWithIcon('success', 'Perfil actualizado exitosamente', <CheckCircleOutlined style={{ color: 'green' }} />);
            closeModal();
        } catch (error) {
            console.log("Failed to update user", error);
        }
    };

    return (
        <>
            <NavBar />
            <div className="form-profile-container">
                <div className="profile-content">
                    <h2 className="profile-header">Perfil de Usuario</h2>
                    <div className="profile-info">
                        <div className="profile-img">
                            <img src={user.imgurl} alt="Imagen de perfil" />
                        </div>
                        <div className="profile-details">
                            <p className="profile-detail"><strong>Nombre de usuario:</strong> {user.username}</p>
                            <p className="profile-detail"><strong>Email:</strong> {user.email}</p>
                        </div>
                    </div>
                    <div className="profile-actions">
                        <button className="btn_p" onClick={handleEditProfile}>Editar información</button>
                        <button className="btn_p" onClick={handlePasswordUpdate}>Cambiar Contraseña</button>
                    </div>
                </div>
                <Modal
                    title="Editar Perfil"
                    visible={isEditProfileModalOpen}
                    onCancel={closeModal}
                    footer={[
                        <Button key="cancel" onClick={closeModal}>
                            Cancelar
                        </Button>,
                        <Button key="submit" type="primary" onClick={handleSaveProfileChanges}>
                            Guardar
                        </Button>,
                    ]}
                >
                    <Form layout="vertical">
                        <Form.Item 
                        label="Nombre de usuario"
                        rules={[{
                            required: true,
                            message: 'Ingrese el nombre de usuario'
                        }]}
                        >
                            <Input value={username} onChange={(e) => setUsername(e.target.value)} />
                        </Form.Item>
                        <Form.Item 
                        label="Email"
                        rules={[{
                            required: true,
                            message: 'Ingrese un correo'
                        }]}
                        >
                            <Input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                        </Form.Item>
                        <Form.Item 
                        label="Imagen de perfil"
                        rules={[{
                            required: true,
                            message: 'Ingrese una imagen'
                        }]}
                        >
                            <Input value={imgurl} onChange={(e) => setImgUrl(e.target.value)} />
                        </Form.Item>
                    </Form>
                </Modal>
                <Modal
                    title="Cambiar Contraseña"
                    visible={isPasswordUpdateModalOpen}
                    onCancel={closeModal}
                    footer={[
                        <Button key="cancel" onClick={closeModal}>
                            Cancelar
                        </Button>,
                        <Button key="submit" type="primary" onClick={handleSaveChanges}>
                            Guardar
                        </Button>,
                    ]}
                >
                    <Form layout="vertical">
                        <Form.Item
                            name="password"
                            label="Nueva Contraseña"
                            rules={[{
                                required: true,
                                message: 'Ingrese una contraseña'
                            }]}
                        >
                            <Input.Password value={password} onChange={(e) => setPassword(e.target.value)} />
                        </Form.Item>
                        <Form.Item
                            name="confirmPassword"
                            label="Confirmar Nueva Contraseña"
                            rules={[{
                                required: true,
                                message: 'Confirme la contraseña'
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('Las contraseñas no son iguales'))
                                },
                            }),
                            ]}>
                            <Input.Password value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        </>
    );
};

export default FormProfile;