import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, Switch, notification } from 'antd';
import { ofertaService } from '../../services/ofertaEducativa';
import { admisionesService } from '../../services/admisones';
import { profesoresService } from '../../services/profesores';
import './OfertaModals.css';

const ModalEdit = ({ isVisible, onCancel, loading, setLoading, oferta, fetchData }) => {
    const [form] = Form.useForm();
    const [admisiones, setAdmisiones] = useState([]);
    const [profesores, setProfesores] = useState([]);

    useEffect(() => {
        if (isVisible) {
            fetchAdmisiones();
            fetchProfesores();
        }
    }, [isVisible]);

    useEffect(() => {
        if (oferta && isVisible) {
            form.setFieldsValue({
                nombre: oferta.nombre,
                estado: oferta.activo,
                admisiones: oferta.admisiones.map(admision => admision._id),
                profesores: oferta.profesores.map(profesor => profesor._id),
            });
        }
    }, [oferta, form, isVisible]);

    const fetchAdmisiones = async () => {
        try {
            const data = await admisionesService.admisiones();
            setAdmisiones(data);
        } catch (error) {
            console.error('Error fetching admisiones:', error);
            notification.error({ message: 'Error al cargar las admisiones', placement: 'bottomRight' });
        }
    };

    const fetchProfesores = async () => {
        try {
            const data = await profesoresService.profesores();
            setProfesores(data);
        } catch (error) {
            console.error('Error fetching profesores:', error);
            notification.error({ message: 'Error al cargar los profesores', placement: 'bottomRight' });
        }
    };

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');

            const ofertaEducativa = {
                nombre: values.nombre,
                activo: values.estado,
            };

            await ofertaService.editOfertaEducativa(token, oferta._id, ofertaEducativa);
            notification.success({ message: 'Oferta educativa actualizada con éxito', placement: 'bottomRight' });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al actualizar oferta educativa:', error);
            notification.error({ message: 'Error al actualizar oferta educativa', placement: 'bottomRight' });
        } finally {
            setLoading(false);
        }
    };

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    return (
        <Modal
            title="Editar Oferta Educativa"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
            centered
        >
            <Form
                form={form}
                name="edit_ofertaEducativa"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Nombre de Oferta"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre de la oferta educativa' }]}
                >
                    <Input className="modal-input" placeholder="Nombre de la oferta educativa" />
                </Form.Item>
                <Form.Item
                    name="estado"
                    label="Estado"
                    valuePropName="checked"
                >
                    <Switch />
                </Form.Item>
                <Form.Item className="modal-footer">
                    <Button onClick={handleCancel} className="modal-btn modal-btn-cancel">
                        Cancelar
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading} className="modal-btn modal-btn-save">
                        Guardar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalEdit;
