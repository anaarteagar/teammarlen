import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, Switch, notification } from 'antd';
import { ofertaService } from '../../services/ofertaEducativa';
import { admisionesService } from '../../services/admisones';
import { profesoresService } from '../../services/profesores';
import './OfertaModals.css';

const ModalAdd = ({ isVisible, onCancel, loading, setLoading, fetchData }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (isVisible) {
            form.resetFields();
        }
    }, [isVisible, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            const ofertaEducativa = {
                nombre: values.nombre,
                activo: values.estado,
            };

            await ofertaService.addOfertaEducativa(token, ofertaEducativa);
            notification.success({ message: 'Oferta educativa creada con éxito', placement: 'bottomRight' });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al crear oferta educativa:', error);
            notification.error({ message: 'Error al crear oferta educativa', placement: 'bottomRight' });
        } finally {
            setLoading(false);
        }
    };

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    return (
        <Modal
            title="Agregar Oferta Educativa"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
            centered
        >
            <Form
                form={form}
                name="add_ofertaEducativa"
                onFinish={onFinish}
                layout="vertical"
            >
                <Form.Item
                    name="nombre"
                    label="Nombre de Oferta"
                    rules={[{ required: true, message: 'Por favor ingrese el nombre de la oferta educativa' }]}
                >
                    <Input className="modal-input" placeholder="Nombre de la oferta educativa" />
                </Form.Item>
                <Form.Item
                    name="estado"
                    label="Estado"
                    valuePropName="checked"
                >
                    <Switch />
                </Form.Item>
                <Form.Item className="modal-footer">
                    <Button onClick={handleCancel} className="modal-btn modal-btn-cancel">
                        Cancelar
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading} className="modal-btn modal-btn-create">
                        Crear Oferta
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalAdd;
