import React from 'react';
import { Modal, Button, notification } from 'antd';
import { ofertaService } from '../../services/ofertaEducativa';
import { DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import './OfertaModals.css';

const ModalDelete = ({ isVisible, onCancel, oferta, loading, setLoading, fetchData }) => {
    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleDelete = async () => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await ofertaService.deleteOferta(token, oferta._id);
            fetchData();
            onCancel();
            openNotificationWithIcon('success', 'Oferta eliminada exitosamente', <DeleteOutlined style={{ color: 'red' }} />);
        } catch (error) {
            console.error('Error al eliminar la oferta:', error);
            openNotificationWithIcon('error', 'Error al eliminar la oferta', <DeleteOutlined style={{ color: 'red' }} />);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title={<span><center>Eliminar Oferta Educativa</center></span>}
            visible={isVisible}
            onCancel={onCancel}
            footer={null}
            centered
        >
            <p>¿Está seguro de que desea eliminar la oferta educativa <strong>{oferta && oferta.nombre}</strong>?</p>
            <div className="button-group">
                <Button key="cancel" onClick={onCancel} className="modal-btn modal-btn-cancel">
                    Cancelar
                </Button>
                <Button
                    key="delete"
                    type="primary"
                    loading={loading}
                    onClick={handleDelete}
                >
                    Eliminar
                </Button>
            </div>
        </Modal>
    );
};

export default ModalDelete;
