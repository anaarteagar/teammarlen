import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Select, notification } from 'antd';
import { admisionesService } from '../../services/admisones';
import { ofertaService } from '../../services/ofertaEducativa';

const { Option } = Select;

const ModalLinkAdmisiones = ({ isVisible, onCancel, loading, setLoading, oferta, fetchData }) => {
    const [form] = Form.useForm();
    const [admisiones, setAdmisiones] = useState([]);

    useEffect(() => {
        if (isVisible) {
            fetchAdmisiones();
        }
    }, [isVisible]);

    const fetchAdmisiones = async () => {
        try {
            const data = await admisionesService.admisiones();
            setAdmisiones(data);
        } catch (error) {
            console.error('Error fetching admisiones:', error);
            notification.error({ message: 'Error al cargar las admisiones', placement: 'bottomRight' });
        }
    };

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            const updatedOferta = {
                admisionId: values.admision,
                ofertaIds: [oferta._id]
            };
            await ofertaService.asigOfertAdmin(token, updatedOferta);
            notification.success({ message: 'Admision vinculada con éxito', placement: 'bottomRight' });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al vincular admision:', error);
            notification.error({ message: 'Error al vincular admision', placement: 'bottomRight' });
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Vincular Admision"
            visible={isVisible}
            onCancel={onCancel}
            footer={null}
        >
            <Form
                form={form}
                name="link_admision"
                onFinish={onFinish}
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}
                initialValues={{
                    admision: oferta && oferta.admisiones && oferta.admisiones.length > 0 ? oferta.admisiones[0]._id : null,
                }}
            >
                <Form.Item
                    name="admision"
                    label="Admision"
                    rules={[{ required: true, message: 'Por favor seleccione una admisión' }]}
                >
                    <Select placeholder="Seleccione una admisión">
                        {admisiones.map(admision => (
                            <Option key={admision._id} value={admision._id}>
                                {admision.nombre}
                            </Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item wrapperCol={{ span: 24 }}>
                    <Button type="primary" htmlType="submit" loading={loading} style={{ width: '140px', marginRight: '16px' }}>
                        Vincular
                    </Button>
                    <Button onClick={onCancel} style={{ width: '140px' }}>
                        Cancelar
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalLinkAdmisiones;
