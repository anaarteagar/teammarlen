import React, { useState } from 'react';
import './NavBarPublic.css';
import { useAuth } from '../../hooks/useAuth';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import logo from '../../assets/img/b7.png';
import ContactModal from '../ModalsCorreo'; 

const NavBar = () => {
    const { user, logout } = useAuth();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const navigate = useNavigate();

    const handleHome = () => {
        navigate('/');
    };

    const handleLogin = () => {
        navigate('/login');
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleSubmit = () => {
        setIsModalVisible(false);
    };

    return (
        <nav className="navbar-public">
            <div className="navbar-brand" onClick={handleHome}>
                <img src={logo} alt="Logo" className="navbar-logo" />
                <span className="navbar-title">LIZARD 2.0</span>
            </div>
            <div className='btn-space'>
                <button onClick={showModal}>
                    <FontAwesomeIcon icon={faEnvelope} /> Contáctanos
                </button>
                <button onClick={handleLogin}>
                    <FontAwesomeIcon icon={faUser} /> Iniciar sesión
                </button>
            </div>

            <ContactModal
                isVisible={isModalVisible}
                onCancel={handleCancel}
                onSubmit={handleSubmit}
            />
        </nav>
    );
};

export default NavBar;