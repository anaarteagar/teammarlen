import React, { useState, useEffect } from 'react';
import { Space, Table, Tag, Button, Input, Modal } from 'antd';
import { useAuth } from '../../hooks/useAuth';
import { admisionesService } from '../../services/admisones';
import { ofertaService } from '../../services/ofertaEducativa';
import ModalAdd from '../ModalsAdmisiones/ModalAdd';
import ModalEdit from '../ModalsAdmisiones/ModalEdit';
import ModalDelete from '../ModalsAdmisiones/ModalDelete';
import NavBar from '../NavBar';
import { DeleteOutlined, EditOutlined, PlusOutlined, CheckCircleOutlined, CloseCircleOutlined, BookOutlined, FilePdfOutlined } from '@ant-design/icons';
import './TableAdmisiones.css';
import generateAdmisionesPDF from '../../services/generateAdmisionesPDF';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';

const { Search } = Input;

const TableAdmisiones = () => {
    const { user } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [selectedOfertas, setSelectedOfertas] = useState([]);
    const [isOfertaModalVisible, setIsOfertaModalVisible] = useState(false);
    const [filteredData, setFilteredData] = useState([]);
    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
    const [selectedAdmisionId, setSelectedAdmisionId] = useState(null);
    const adminRoleId = '66789c052ed810254bf43470'; // ID del rol de admin

    useEffect(() => {
        document.title = 'Admisiones';
        changeFavicon(InicioFavicon);
    }, []);

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const showModalEdit = (admisionId) => {
        setSelectedAdmisionId(admisionId);
        setIsModalEditOpen(true);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
    };

    const showModalDelete = (admisionId) => {
        setSelectedAdmisionId(admisionId);
        setIsModalDeleteOpen(true);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
    };

    const fetchAdmisionesYOfertas = async () => {
        try {
            const token = localStorage.getItem('token');
            const [admisionesData, ofertasData] = await Promise.all([
                admisionesService.admisiones(token),
                ofertaService.getOferta(token)
            ]);

            const formattedData = admisionesData.map((admission, index) => {
                const relatedOfertas = (admission.ofertasEducativas || []).map(ofertaName =>
                    ofertasData.find(oferta => oferta.nombre === ofertaName)
                );
                return {
                    ...admission,
                    key: index + 1,
                    ofertas: relatedOfertas
                };
            });

            setData(formattedData);
            setFilteredData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener las admisiones y ofertas educativas:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchAdmisionesYOfertas();
    }, []);

    const handleViewOfertas = (ofertas) => {
        if (ofertas.length > 0) {
            setSelectedOfertas(ofertas);
            setIsOfertaModalVisible(true);
        } else {
            console.warn('No se pudieron obtener las ofertas educativas.');
        }
    };

    const handleCloseOfertaModal = () => {
        setIsOfertaModalVisible(false);
        setSelectedOfertas([]);
    };

    const handleSearch = (e) => {
        const value = e.target.value.toLowerCase();
        const filtered = data.filter(item => {
            const isNameMatch = item.nombre.toLowerCase().includes(value);
            const isEstadoMatch = value === 'activa' && item.activo ||
                                 value === 'inactiva' && !item.activo;
            const isOfertasMatch = item.ofertas.some(oferta => oferta && oferta.nombre.toLowerCase().includes(value));
            return isNameMatch || isEstadoMatch || isOfertasMatch;
        });
        setFilteredData(filtered);
    };

    const handleGeneratePDF = () => {
        generateAdmisionesPDF(filteredData, user.username);
    };

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            key: '_id',
            align: 'left',
        },
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            align: 'left',
        },
        {
            title: 'Activo',
            dataIndex: 'activo',
            key: 'activo',
            align: 'center',
            render: status => (
                <Tag color={status ? 'green' : 'red'}>
                    {status ? (
                        <><CheckCircleOutlined /> Activa</>
                    ) : (
                        <><CloseCircleOutlined /> Inactiva</>
                    )}
                </Tag>
            ),
        },
        {
            title: 'Ofertas Educativas',
            key: 'ofertas',
            align: 'center',
            render: (_, record) => (
                <Button
                    onClick={() => handleViewOfertas(record.ofertas)}
                    disabled={record.ofertas.length === 0}
                    type="primary"
                    icon={<BookOutlined />}
                >
                    Ver Ofertas
                </Button>
            ),
        }
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                <Space>
                    <Button className="edit-button" icon={<EditOutlined />} onClick={() => showModalEdit(record._id)} />
                    <Button className="delete-button" icon={<DeleteOutlined />} onClick={() => showModalDelete(record._id)} />
                </Space>
            ),
        });
    }

    return (
        <>
            <NavBar />
            <div className='container'>
                <div className="table-header">
                    <div className="table-header-title">
                        <h3>Admisiones</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Input
                                    placeholder="Buscar admisiones"
                                    onChange={handleSearch}
                                    style={{ width: 200 }}
                                />
                                <Button icon={<PlusOutlined />} onClick={showModalAdd}>
                                    Agregar
                                </Button>
                                <Button onClick={handleGeneratePDF}>
                                    <FilePdfOutlined /> Generar PDF
                                </Button>
                            </Space>
                        )}
                    </div>
                    <Table
                        columns={columns}
                        dataSource={filteredData}
                        loading={loading}
                        pagination={{ pageSize: 6 }}
                        scroll={{ y: 260 }}
                    />
                </div>
            </div>
            <Modal
                title="Detalles de las Ofertas Educativas"
                visible={isOfertaModalVisible}
                onCancel={handleCloseOfertaModal}
                footer={null}
                width={800}
            >
                <Table
                    dataSource={selectedOfertas}
                    columns={[
                        {
                            title: 'Nombre',
                            dataIndex: 'nombre',
                            key: 'nombre',
                        },
                        {
                            title: 'Estado',
                            dataIndex: 'activo',
                            key: 'activo',
                            render: (activo) => (
                                <Tag color={activo ? 'green' : 'red'}>
                                    {activo ? 'Activa' : 'Inactiva'}
                                </Tag>
                            ),
                        },
                    ]}
                    pagination={false}
                />
            </Modal>
            <ModalAdd
                isVisible={isModalAddOpen}
                onCancel={handleCancelAdd}
                fetchData={fetchAdmisionesYOfertas}
            />
            <ModalEdit
                admisionId={selectedAdmisionId}
                isVisible={isModalEditOpen}
                onCancel={handleCancelEdit}
                fetchData={fetchAdmisionesYOfertas}
            />
            <ModalDelete
                admisionId={selectedAdmisionId}
                isVisible={isModalDeleteOpen}
                onCancel={handleCancelDelete}
                fetchData={fetchAdmisionesYOfertas}
            />
        </>
    );
};

export default TableAdmisiones;