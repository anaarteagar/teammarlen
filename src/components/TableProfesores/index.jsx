import React, { useState, useEffect } from 'react';
import moment from 'moment';
import NavBar from '../NavBar';
import { Space, Table, Tag, Button, Input } from 'antd';
import { useAuth } from '../../hooks/useAuth';
import { profesoresService } from '../../services/profesores';
import { PlusOutlined, EditOutlined, DeleteOutlined, FilePdfOutlined } from '@ant-design/icons';
import ModalAdd from '../ModalsTeachers/ModalAdd';
import ModalEdit from '../ModalsTeachers/ModalEdit';
import ModalDelete from '../ModalsTeachers/ModalDelete';
import generateProfesoresPDF from '../../services/generateProfesoresPDF';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';

const TableProfesores = () => {
    const { user, logout } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const [hasSearchResults, setHasSearchResults] = useState(true);

    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
    const adminRoleId = '66789c052ed810254bf43470';

    const [selectedTeacher, setSelectedTeacher] = useState(null);

    useEffect(() => {
        document.title = 'Profesores';
        changeFavicon(InicioFavicon);
    }, []);

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const showModalEdit = (user) => {
        setSelectedTeacher(user);
        setIsModalEditOpen(true);
    };

    const showModalDelete = (user) => {
        setSelectedTeacher(user);
        setIsModalDeleteOpen(true);
    };

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
        setSelectedTeacher(null);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
        setSelectedTeacher(null);
    };

    const fetchTeachers = async () => {
        try {
            const token = localStorage.getItem('token');
            const usersData = await profesoresService.profesores(token);
            const formattedData = usersData.map((item, index) => ({
                ...item,
                key: index + 1,
                fechaNacimiento: moment(item.fechaNacimiento).format('DD/MM/YYYY'), 
            }));
            setData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener los profesores:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchTeachers();
    }, []);

    const handleGeneratePDF = () => {
        generateProfesoresPDF(filteredData, user.username);
    };

    const handleSearch = (e) => {
        setSearchText(e.target.value);
    };

    const filteredData = data.filter(teacher =>
        teacher.nombre.toLowerCase().includes(searchText.toLowerCase()) ||
        teacher.apellidos.toLowerCase().includes(searchText.toLowerCase()) ||
        teacher.numeroEmpleado.toLowerCase().includes(searchText.toLowerCase()) ||
        teacher.fechaNacimiento.toLowerCase().includes(searchText.toLowerCase()) ||
        teacher.correo.toLowerCase().includes(searchText.toLowerCase())
    );

    useEffect(() => {
        setHasSearchResults(filteredData.length > 0);
    }, [filteredData]);

    const columns = [
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            align: 'left',
        },
        {
            title: 'Apellidos',
            dataIndex: 'apellidos',
            key: 'apellidos',
            align: 'left',
        },
        {
            title: 'No. empleado',
            dataIndex: 'numeroEmpleado',
            key: 'numeroEmpleado',
            align: 'left',
        },
        {
            title: 'Correo electrónico',
            dataIndex: 'correo',
            key: 'correo',
            align: 'left',
        },
        {
            title: (
                <span>
                    Fecha de Nacimiento
                    <br />
                    (dd/mm/yyyy)
                </span>
            ),
            dataIndex: 'fechaNacimiento',
            key: 'fechaNacimiento',
            align: 'left',
        },
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                <Space>
                    <Button className="edit-button" onClick={() => showModalEdit(record)}>
                        <EditOutlined />
                    </Button>
                    <Button className="delete-button" onClick={() => showModalDelete(record)}>
                        <DeleteOutlined />
                    </Button>
                </Space>
            )
        });
    }

    const locale = {
        emptyText: hasSearchResults ? 'Cargando' : 'No se han encontrado coincidencias',
    };

    return (
        <>
            <NavBar />
            <div className='container'>
                <div className="table-header">
                    <div className="table-header-title">
                        <h3>Profesores</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Input 
                                    placeholder="Buscar profesores" 
                                    value={searchText} 
                                    onChange={handleSearch} 
                                    style={{ width: 200, marginRight: 10 }} 
                                />
                                <Button className="add-button2" onClick={showModalAdd}>
                                    <PlusOutlined /> Agregar
                                </Button>
                                <Button onClick={handleGeneratePDF}>
                                    <FilePdfOutlined /> Generar PDF
                                </Button>
                            </Space>
                        )}
                    </div>
                    <Table
                        columns={columns}
                        dataSource={filteredData}
                        loading={loading}
                        pagination={{ pageSize: 6 }}
                        scroll={{ y: 260 }}
                        locale={locale}
                    />
                </div>
                <ModalAdd
                    isVisible={isModalAddOpen}
                    onCancel={handleCancelAdd}
                    loading={loading}
                    setLoading={setLoading}
                    fetchData={fetchTeachers}
                />
                <ModalEdit
                    isVisible={isModalEditOpen}
                    onCancel={handleCancelEdit}
                    loading={loading}
                    setLoading={setLoading}
                    user={selectedTeacher}
                    fetchData={fetchTeachers}
                />
                <ModalDelete
                    isVisible={isModalDeleteOpen}
                    onCancel={handleCancelDelete}
                    loading={loading}
                    setLoading={setLoading}
                    user={selectedTeacher}
                    fetchData={fetchTeachers}
                />
            </div>
        </>
    )
}

export default TableProfesores;
