import React from 'react';
import { Modal, Button, notification } from 'antd';
import { usersService } from '../../services/users';
import { DeleteOutlined } from '@ant-design/icons';

const ModalDelete = ({ isVisible, onCancel, user, loading, setLoading, fetchData }) => {
    const openNotificationWithIcon = (type, message, icon) => {
        notification[type]({
            message: message,
            icon: icon,
        });
    };

    const handleDelete = async () => {
        setLoading(true);
        try {
            const token = localStorage.getItem('token');
            await usersService.deleteUser(token, user._id);
            fetchData();
            onCancel();
            openNotificationWithIcon('success', 'Usuario eliminado exitosamente', <DeleteOutlined style={{ color: 'red' }} />);
        } catch (error) {
            console.error('Error al eliminar el usuario:', error);
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Eliminar Usuario"
            visible={isVisible}
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancelar
                </Button>,
                <Button key="delete"  type="primary" loading={loading} onClick={handleDelete} className="modal-button submit-button">
                    Eliminar
                </Button>,
            ]}
        >
            <p>¿Está seguro de que desea eliminar a {user && user.username} con correo {user && user.email}?</p>
        </Modal>
    );
};

export default ModalDelete;
