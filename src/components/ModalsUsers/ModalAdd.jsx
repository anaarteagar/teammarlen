import React, { useState, useEffect } from 'react';
import { Modal, Button, Form, Input, Select, message } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import axios from 'axios';
import './UserModals.css';

const ModalAdd = ({ isVisible, onCancel, loading, setLoading, fetchData }) => {
    const [form] = Form.useForm();
    const roles = ['admin', 'customer']; 

    useEffect(() => {
        if (isVisible) {
            form.resetFields();
        }
    }, [isVisible, form]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const dataToSend = {
                ...values,
                roles: values.roles ? values.roles : []
            };
            
            const response = await axios.post('https://lizard2-m2evbcgb8-ricolin2653ms-projects.vercel.app/api/auth/signup', dataToSend);
            if (response.status === 200) {
                message.success('Usuario creado con éxito');
                fetchData(); // Invoca fetchUsers para actualizar la lista
                onCancel();
            }
        } catch (error) {
            console.error('Error al crear usuario:', error);
            message.error('Error al crear usuario');
        } finally {
            setLoading(false);
        }
    };

    const handleCancel = () => {
        form.resetFields();
        onCancel();
    };

    return (
        <Modal
            title="Agregar Nuevo Usuario"
            visible={isVisible}
            onCancel={handleCancel}
            footer={null}
        >
            <Form form={form} onFinish={onFinish} layout="vertical">
                <Form.Item
                    name="username"
                    label="Nombre de usuario"
                    rules={[{ required: true, message: 'Por favor ingrese un nombre de usuario' }]}
                >
                    <Input className="input-field"/>
                </Form.Item>
                <Form.Item
                    name="email"
                    label="Correo electrónico"
                    rules={[
                        { required: true, message: 'Por favor ingrese un correo electrónico' },
                        { type: 'email', message: 'Por favor ingrese un correo electrónico válido' }
                    ]}
                >
                    <Input className="input-field"/>
                </Form.Item>
                <Form.Item      
                    name="password"
                    label="Contraseña"
                    rules={[{ required: true, message: 'Por favor ingrese una contraseña' }]}
                >
                    <Input.Password className="input-field"/>
                </Form.Item>
                <Form.Item
                    name="roles"
                    label="Roles"
                >
                    <Select 
                        mode="multiple" 
                        placeholder="Seleccione los roles"
                        clearIcon={<CloseOutlined />}
                        allowClear
                        className="input-field"
                    >
                        {roles.map(role => (
                            <Select.Option key={role} value={role}>{role}</Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item className="form-buttons">
                    <Button onClick={handleCancel} className="modal-button cancel-button">
                        Cancelar
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading} className="modal-button submit-button">
                        Crear Usuario
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalAdd;
