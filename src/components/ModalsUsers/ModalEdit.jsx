import { Modal, Input, Form, notification, Checkbox, Button } from 'antd';
import { useEffect } from 'react';
import { usersService } from '../../services/users';
import './UserModals.css';

const adminRoleId = '66789c052ed810254bf43470';
const userRoleId = '66789c052ed810254bf4346f';

const ModalEdit = ({ isVisible, onCancel, loading, setLoading, user, fetchData }) => {
    const [form] = Form.useForm();

    useEffect(() => {
        if (user) {
            form.setFieldsValue({
                username: user.username,
                email: user.email,
                imgurl: user.imgurl,
                roles: user.roles.map(role => role === adminRoleId ? 'admin' : role === userRoleId ? 'customer' : role),
            });
        }
    }, [user, form]);

    const roleOptions = [
        { label: 'Admin', value: 'admin' },
        { label: 'User', value: 'customer' },
    ];

    const onFinish = async (values) => {
        setLoading(true);
        try {
            //console.log('Datos a enviar al backend:', values);

            const token = localStorage.getItem('token');

            const userData = {
                username: values.username,
                email: values.email,
                imgurl: values.imgurl,
                roles: values.roles
            };

            await usersService.updateUser(token, user._id, userData);

            notification.success({ message: 'Usuario actualizado correctamente' });
            fetchData();
            onCancel();
        } catch (error) {
            console.error('Error al actualizar usuario:', error);
            notification.error({ message: 'Error al actualizar el usuario' });
        } finally {
            setLoading(false);
        }
    };

    return (
        <Modal
            title="Editar usuario"
            visible={isVisible}
            onCancel={onCancel}
            footer={[
                <div className="form-buttons" key="buttons">
                    <Button key="cancel" onClick={onCancel} className="modal-button cancel-button">
                        Cancelar
                    </Button>,
                    <Button key="submit" type="primary" onClick={() => form.submit()} className="modal-button submit-button">
                        Guardar
                    </Button>
                </div>
            ]}
        >
            <div className="modal-content">
                <Form
                    form={form}
                    name="edit_user"
                    className="edit-user-form"
                    initialValues={{}}
                    onFinish={onFinish}
                    layout="vertical"
                >
                    <Form.Item
                        label="Nombre de usuario"
                        name="username"
                        rules={[
                            { required: true, message: 'Por favor ingrese su nombre de usuario' }
                        ]}
                    >
                        <Input className="input-field" placeholder="Nombre de usuario" />
                    </Form.Item>

                    <Form.Item
                        label="Correo electrónico"
                        name="email"
                        rules={[
                            { required: true, message: 'Por favor ingrese su correo electrónico' },
                            { type: 'email', message: 'Por favor ingrese un correo electrónico válido' }
                        ]}
                    >
                        <Input className="input-field" placeholder="Correo electrónico" />
                    </Form.Item>

                    <Form.Item
                        label="URL de la imagen"
                        name="imgurl"
                        rules={[
                            { required: false, message: 'Por favor ingrese la URL de la imagen' }
                        ]}
                    >
                        <Input className="input-field" placeholder="URL de la imagen" />
                    </Form.Item>

                    <Form.Item
                        label="Roles"
                        name="roles"
                        rules={[
                            { required: true, message: 'Por favor seleccione al menos un rol' }
                        ]}
                    >
                        <Checkbox.Group options={roleOptions} />
                    </Form.Item>
                </Form>
            </div>
        </Modal>
    );
};

export default ModalEdit;
