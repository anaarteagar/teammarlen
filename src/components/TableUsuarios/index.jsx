import React, { useState, useEffect } from 'react';
import { Space, Table, Tag, Button, Input } from 'antd';
import { useAuth } from '../../hooks/useAuth';
import { usersService } from '../../services/users';
import ModalAdd from '../ModalsUsers/ModalAdd';
import ModalEdit from '../ModalsUsers/ModalEdit';
import ModalDelete from '../ModalsUsers/ModalDelete';
import NavBar from '../NavBar';
import { PlusOutlined, EditOutlined, DeleteOutlined, FilePdfOutlined, SearchOutlined } from '@ant-design/icons';
import './TableUsuarios.css';
import generatePDF from '../../services/generatePDF';
import { changeFavicon } from '../../utils/changeFavicon';
import InicioFavicon from '../../assets/img/favicon.ico';

const TableUsers = () => {
    const { user, logout } = useAuth();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const [hasSearchResults, setHasSearchResults] = useState(true); // Estado para controlar si hay resultados de búsqueda

    const adminRoleId = '66789c052ed810254bf43470';
    const userRoleId = '66789c052ed810254bf4346f';

    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [isModalEditOpen, setIsModalEditOpen] = useState(false);
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);

    const [selectedUser, setSelectedUser] = useState(null);

    useEffect(() => {
        document.title = 'Usuarios';
        changeFavicon(InicioFavicon);
    }, []);

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const showModalEdit = (user) => {
        setSelectedUser(user);
        setIsModalEditOpen(true);
    };

    const showModalDelete = (user) => {
        setSelectedUser(user);
        setIsModalDeleteOpen(true);
    };

    const handleCancelAdd = () => {
        setIsModalAddOpen(false);
    };

    const handleCancelEdit = () => {
        setIsModalEditOpen(false);
        setSelectedUser(null);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
        setSelectedUser(null);
    };

    const fetchUsers = async () => {
        try {
            const token = localStorage.getItem('token');
            const usersData = await usersService.users(token);
            const formattedData = usersData.map((item, index) => ({
                ...item,
                key: index + 1,
            }));
            setData(formattedData);
            setLoading(false);
        } catch (error) {
            console.error('Error al obtener los usuarios:', error);
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchUsers();
    }, []);

    const handleLogout = () => {
        logout();
    };

    const handleGeneratePDF = () => {
        generatePDF(filteredData, user.username);
    };

    const handleSearch = (e) => {
        setSearchText(e.target.value);
    };

    const filteredData = data.filter(user =>
        user.username.toLowerCase().includes(searchText.toLowerCase()) ||
        user.email.toLowerCase().includes(searchText.toLowerCase()) ||
        user.roles.some(role =>
            (role === adminRoleId && 'admin'.includes(searchText.toLowerCase())) ||
            (role === userRoleId && 'user'.includes(searchText.toLowerCase()))
        )
    );

    useEffect(() => {
        setHasSearchResults(filteredData.length > 0); // Actualiza el estado de hasSearchResults basado en si hay resultados filtrados
    }, [filteredData]);

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            key: '_id',
            align: 'left',
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
            align: 'left',
        },
        {
            title: 'Correo electrónico',
            dataIndex: 'email',
            key: 'email',
            align: 'left',
        },
        {
            title: 'Roles',
            dataIndex: 'roles',
            key: 'roles',
            align: 'left',
            render: roles => (
                roles.map(role => (
                    <Tag color="blue" key={role}>
                        {role === adminRoleId ? 'admin' : role === userRoleId ? 'user' : role}
                    </Tag>
                ))
            ),
        },
    ];

    if (user && user.roles && user.roles.includes(adminRoleId)) {
        columns.push({
            title: 'Acciones',
            key: 'acciones',
            align: 'center',
            render: (_, record) => (
                user.email !== record.email && (
                    <Space>
                        <Button className="edit-button" onClick={() => showModalEdit(record)}>
                            <EditOutlined />
                        </Button>
                        <Button className="delete-button" onClick={() => showModalDelete(record)}>
                            <DeleteOutlined />
                        </Button>
                    </Space>
                )
            ),
        });
    }

    const locale = {
        emptyText: hasSearchResults ? 'Cargando' : 'No se han encontrado coincidencias', // Personaliza el mensaje cuando no hay datos filtrados
    };

    return (
        <>
            <NavBar />
            <div className='container'>
                <div className="table-header">
                    <div className="table-header-title">
                        <h3>Usuarios</h3>
                        {user && user.roles && user.roles.includes(adminRoleId) && (
                            <Space>
                                <Input 
                                    placeholder="Buscar usuarios" 
                                    value={searchText} 
                                    onChange={handleSearch} 
                                    style={{ width: 200, marginRight: 10 }} 
                                />
                                <Button className="add-button2" onClick={showModalAdd}>
                                    <PlusOutlined /> Agregar
                                </Button>
                                <Button onClick={handleGeneratePDF}>
                                    <FilePdfOutlined />Generar PDF
                                </Button>
                            </Space>
                        )}
                        </div>
                        <Table
                            columns={columns}
                            dataSource={filteredData}
                            loading={loading}
                            pagination={{ pageSize: 6 }}
                            locale={locale} 
                            scroll={{ y: 260 }}
                        />
                    </div>
                    <ModalAdd
                        isVisible={isModalAddOpen}
                        onCancel={handleCancelAdd}
                        loading={loading}
                        setLoading={setLoading}
                        fetchData={fetchUsers}
                    />
                    <ModalEdit
                        isVisible={isModalEditOpen}
                        onCancel={handleCancelEdit}
                        loading={loading}
                        setLoading={setLoading}
                        user={selectedUser}
                        fetchData={fetchUsers}
                    />
                    <ModalDelete
                        isVisible={isModalDeleteOpen}
                        onCancel={handleCancelDelete}
                        loading={loading}
                        setLoading={setLoading}
                        user={selectedUser}
                        fetchData={fetchUsers}
                    />
                </div>
            </>
        );
    };
    
export default TableUsers;
