import React from 'react';
import { useRoutes } from 'react-router-dom';
import Home from '../pages/Home';
import HomePublic from '../pages/HomePublic';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Profile from '../pages/Profile';
import ProfileUpdate from '../pages/ProfileUpdate'
import PasswordUpdate from '../pages/PasswordUpdate';
import TableAdmisiones from '../components/TableAdmisiones';
import TableUsers from '../components/TableUsuarios';
import TableTeachers from '../components/TableProfesores';
import Tableoferta from '../components/TableofertaEducativa';
import { useAuth } from '../hooks/useAuth';
import TableMateria from '../components/TableMateria';
import PublicEndpoint from '../pages/PublicEndpoint';
import TableCuatrimestre from '../components/TableCuatrimestre';
const adminRoleId = '66789c052ed810254bf43470';

const AppRoutes = () => {
    const { user } = useAuth();

    let routes = useRoutes([
        { path: '/', element: user ? <Home /> : <PublicEndpoint /> },
        { path: '/admisiones', element: <TableAdmisiones /> },
        { path: '/usuarios', element: user && user.roles && user.roles.includes(adminRoleId) ? <TableUsers /> :<Home/> },
        { path: '/login', element: <Login /> },
        { path: '/register', element: <Register /> },
        { path: '/profile', element: <Profile /> },
        { path: '/profileUpdate', element: <ProfileUpdate /> },
        { path: '/passwordUpdate', element: <PasswordUpdate /> },
        { path: '/profesores', element: <TableTeachers /> },
        { path: '/oferta', element: <Tableoferta /> },
        { path: '/materias', element: <TableMateria/> },
        { path: '/inicio', element: <PublicEndpoint /> },
        { path: '/cuatrimestre', element:  <TableCuatrimestre/>},
    ]);

    return routes;
};

export default AppRoutes;
