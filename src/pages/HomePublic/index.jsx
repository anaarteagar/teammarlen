import React from 'react';
import { useAuth } from '../../hooks/useAuth';
import { Button } from 'antd';
import NavBar from '../../components/NavBar';

const HomePublic = () => {
    const { user, admisiones,  logout } = useAuth();
    return (
        <>
        <div>
        <NavBar />
        </div>
        </>
    );
};

export default HomePublic;
