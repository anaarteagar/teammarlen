import React from 'react';
import { useAuth } from '../../hooks/useAuth';
import { Button } from 'antd';
import NavBar from '../../components/NavBar';
import './Home.css';

const Home = () => {
    const { user, admisiones, logout } = useAuth();
    return (
        <>
            <NavBar />
            <div className="home-container">
                <div className="containerH">
                    <div className="home-content">
                        <div className="text-content">
                            <h1>Bienvenido al Portal de Admisiones</h1>
                            <p>Consulta las listas de admisiones y descubre todo lo que necesitas saber para tu ingreso a la universidad.</p>
                            <Button type="primary" size="large" onClick={() => window.location.href = '/Admisiones'}>Ver Listas de Admisiones</Button>
                        </div>
                        <div className="image-content">
                            <img src="https://www.liderempresarial.com/wp-content/uploads/2021/12/UTEQ-diplomado-Industria-4.0-ch.jpg" alt="UTEQ" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Home;
