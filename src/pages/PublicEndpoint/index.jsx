import React from 'react';
import NavBarPublic from '../../components/NavbarPublic';
import ComponenteDescripcion from '../../components/InicioDescripcion';
import ComponenteDivisiones from '../../components/InicioDivisiones';
import './PublicEndpoint.css';

const PublicEndpoint = () => {
    return (
        <>
        <NavBarPublic />
            <div className="home-container">
                <ComponenteDescripcion />
                <ComponenteDivisiones />
            </div>
        </>
    );
};

export default PublicEndpoint;

