import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const materias = async (token) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.MATERIA}`;
        const response = token ? await authFetch(url) : await fetch(url); // Usar authFetch solo si hay token
        if (!response.ok) {
            throw new Error('Error al obtener las materias');
        }
        return await response.json();
    } catch (error) {
        console.error('Error en función naterias:', error);
        throw new Error('Error al obtener las materias');
    }
};

const addMateria = async (token, materia) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.MATERIA}`;
        const response = await authFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(materia),
        });
        if (!response.ok) {
            throw new Error('Error al agregar la materia');
        }

        return await response.json();
    } catch (error) {
        console.error("Error al dar el alta la materia", error);
        throw error;
    }
}

const editMateria = async (token, idMateria, dataMateria) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.MATERIA}/${idMateria}`;
        const response = await authFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(dataMateria),
        });
        if (!response.ok) {
            throw new Error('Error al editar la materia');
        }

        return await response.json();
    } catch ( error ) {
        console.error('Error al editar la materia: ', error);
        throw error;
    }
}

const deleteMateria = async (token, id) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.MATERIA}/${id}`;
        const response = await authFetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            }
        })

        if (!response.ok) {
            throw new Error('Error al eliminar la materia');
        }

        return await response.json();
    } catch (error) {
        console.error("Error al eliminar la materia: ", error);
        throw new Error("Error al eliminar la materia");
    }
}

export const materiaServices = {
    materias,
    addMateria,
    editMateria,
    deleteMateria
}