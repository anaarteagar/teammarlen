import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const getCuatrimestre = async (token) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.CUATRIMESTRE}`;
        const response = token ? await authFetch(url) : await fetch(url); // Usar authFetch solo si hay token
        if (!response.ok) {
            throw new Error('Error al obtener los cuatrimestres');
        }
        return await response.json();
    } catch (error) {
        console.error('Error en función cuatrimestre:', error);
        throw new Error('Error al obtener los cuatrimestres');
    }
}

const addCuatrimestre = async (token, cuatrimestre) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.CUATRIMESTRE}`;
        const response = await authFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(cuatrimestre),
        });

        if(!response.ok) {
            throw new Error('Error al agregar el cuatrimestre');
        }

        return await response.json();
    } catch (error) {
        console.error("Error al dar el alta el cuatrimestre", error);
        throw error;
    }
}

const editCuatrimestre = async (token,  cuatrimestreId, cuatrimestreData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.CUATRIMESTRE}/${cuatrimestreId}`;
        const response = await authFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(cuatrimestreData),
        });

        if (!response.ok) {
            throw new Error('Error al actualizar el cuatrimestre');
        }

        return await response.json();
    } catch (error) {
        console.error('Error al actualizar el cuatrimestre:', error);
        throw error;
    }
}

const deleteCuatrimestre = async (token, cuatrimestreId) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.CUATRIMESTRE}/${cuatrimestreId}`;
        const response = await authFetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
        });

        if (!response.ok) {
            throw new Error('Error al eliminar el cuatrimestre');
        }

        return await response.json();
    } catch (error) {
        console.error('Error al eliminar el cuatrimestre:', error);
        throw new Error('Error al eliminar el cuatrimestre');
    }
}

export const cuatrimestreServices = {
    getCuatrimestre,
    addCuatrimestre,
    editCuatrimestre,
    deleteCuatrimestre
}