import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generatePDF = (users, userGeneratingReport) => {
    const adminRoleId = '66789c052ed810254bf43470';
    const userRoleId = '66789c052ed810254bf4346f';

    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Usuarios', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);
    doc.text(`Generado por: ${userGeneratingReport}`, 14, 40);

    const headers = [
        ['ID', 'Username', 'Email', 'Roles']
    ];

    const rows = users.map(user => [
        user._id,
        user.username,
        user.email,
        user.roles.map(role => {
            if (role === adminRoleId) return 'admin';
            if (role === userRoleId) return 'user';
            return 'unknown';
        }).join(', ')
    ]);

    doc.autoTable({
        head: headers,
        body: rows,
        startY: 50,
    });

    doc.save('ReporteUsuarios.pdf');
};

export default generatePDF;
