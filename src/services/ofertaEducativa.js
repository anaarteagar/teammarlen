import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const getOferta = async (token) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}`;
        const response = token ? await authFetch(url) : await fetch(url); 
        if (!response.ok) {
            throw new Error('Error al obtener las ofertas');
        }
        return await response.json();
    } catch (error) {
        console.error('Error en función ofertas:', error);
        throw new Error('Error al obtener las ofertas');
    }
};

const addOfertaEducativa = async (token, ofertData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}`;
        const response = await authFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(ofertData),
        });
        if (!response.ok) {
            throw new Error('Error al agregar la oferta');
        }

        return await response.json();
    } catch (error) {
        console.log("Error al agregar oferta", error);
        throw error;
    }
}

const editOfertaEducativa = async (token, id, ofertData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}/${id}`;
        const response = await authFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(ofertData),
        });

        return await response.json();
    } catch (error) {
        console.log("Error al ediatr oferta", error);
        throw error;
    }
}

const viewOferta = async (id) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}/${id}`;
        const reponse = await authFetch(url, {
            method: "GET"
        });
        return await reponse.json();
    } catch (error) {
        console.error('Error al obtener la oferta: ', error);
        throw error;
    }
}

const deleteOferta = async (token, id) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}/${id}`;
        const response = await authFetch(url, {
            method: 'DELETE',
            headers: {
                'x-access-token': token
            },
        });
        if (!response.ok) {
            throw new Error('Error al eliminar el profesor');
        }

        return await response.json();
    } catch (error) {
        console.log("Error al eliminar la oferta: ", error);
        throw new Error('Error al eliminar la oferta');
    }
}

const asigOfertAdmin = async (token, data) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.VINCULAR}`;
        const response = await authFetch(url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(data),
        });
        if (!response.ok) {
            throw new Error('Error al vincular la oferta');
        }

        return await response.json();
    } catch (error) {
        console.log("Error al vincular  la oferta: ", error);
        throw new Error('Error al vincular la oferta');
    }
}
const getTeachersByOferta = async (token, ofertaId) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.OFERTA}/${ofertaId}/teachers`;
        const response = await authFetch(url, {
            method: 'GET',
            headers: {
                'x-access-token': token
            },
        });
        if (!response.ok) {
            throw new Error('Error al obtener los profesores de la oferta');
        }
        return await response.json();
    } catch (error) {
        console.error('Error al obtener los profesores de la oferta:', error);
        throw new Error('Error al obtener los profesores de la oferta');
    }
};

export const ofertaService = {
    getOferta,
    addOfertaEducativa,
    editOfertaEducativa,
    deleteOferta,
    asigOfertAdmin,
    viewOferta,
    getTeachersByOferta  
}