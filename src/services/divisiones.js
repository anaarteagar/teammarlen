import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const divisiones = async (token) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.DIVISIONES}`;
        const response = token ? await authFetch(url) : await fetch(url); // Usar authFetch solo si hay token
        if (!response.ok) {
            throw new Error('Error al obtener las divisiones');
        }
        return await response.json();
    } catch (error) {
        console.error('Error en funcion divisiones:', error);
        throw new Error('Error al obtener las divisiones');
    }
};

const getDivision = async (token, divisionId) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.DIVISIONES}/${divisionId}`;
        const response = await authFetch(url, {
            headers: {
                'x-access-token': token
            }
        });
        if (!response.ok) {
            throw new Error('Error al obtener division');
        }
        const data = await response.json();
        console.log(data); // Imprimir el objeto JSON
        return data;
    } catch (error) {
        console.error('Error en función get division:', error);
        throw new Error('Error al obtener la division');
    }
};

const addDivision = async (token, divisionData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.DIVISIONES}`;
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token 
            },
            body: JSON.stringify(divisionData)
        });
        return await response.json();
    } catch (error) {
        console.error('Error en función agregar division:', error);
        throw new Error('Error al agregar la división');
    }
};

const updateDivision = async (token, divisionId, updatedData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.DIVISIONES}/${divisionId}`;
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token 
            },
            body: JSON.stringify(updatedData)
        });
        return await response.json();
    } catch (error) {
        console.error('Error en función update division:', error);
        throw new Error('Error al actualizar la division');
    }
};

const deleteDivision = async (token, divisionId) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.DIVISIONES}/${divisionId}`;
        
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token 
            }
        });

        if (!response.ok) {
            throw new Error('Error en la solicitud de eliminación');
        }

        return await response.json();
    } catch (error) {
        console.error('Error en función delete division:', error);
        throw new Error('Error al eliminar la division');
    }
};

export const divisionesService = {
    divisiones,
    getDivision,
    addDivision,
    updateDivision,
    deleteDivision
};