import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generateProfesoresPDF = (profesores, profesorGeneratingReport) => {
    const adminRoleId = '66789c052ed810254bf43470';
    const userRoleId = '66789c052ed810254bf4346f';

    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Profesores', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);
    doc.text(`Generado por: ${profesorGeneratingReport}`, 14, 40);

    const headers = [
        ['ID', 'Nombre', 'Apellidos', 'No. Empleado', 'Correo', "Fecha de Nacimiento"]
    ];

    const rows = profesores.map(profesor => [
        profesor._id,
        profesor.nombre,
        profesor.apellidos,
        profesor.numeroEmpleado,
        profesor.correo,
        profesor.fechaNacimiento
    ]);

    doc.autoTable({
        head: headers,
        body: rows,
        startY: 50,
    });

    doc.save('ReporteProfesores.pdf');
};

export default generateProfesoresPDF;
