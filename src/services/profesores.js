import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const profesores = async (token) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.PROFESORES}`;
        const response = token ? await authFetch(url) : await fetch(url); // Usar authFetch solo si hay token
        if (!response.ok) {
            throw new Error('Error al obtener los profesores');
        }
        return await response.json();
    } catch (error) {
        console.error('Error en función profesores:', error);
        throw new Error('Error al obtener las profesores');
    }
};

const addProfesor = async (token, profesor) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.PROFESORES}`;
        const response = await authFetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(profesor),
        });

        if(!response.ok) {
            throw new Error('Error al agregar al profesor');
        }

        return await response.json();
    } catch (error) {
        console.error("Error al dar el alta del profesor", error);
        throw error;
    }
};

const updateProfesor = async (token, profesorId, profesorData) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.PROFESORES}/${profesorId}`;
        const response = await authFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
            body: JSON.stringify(profesorData),
        });

        if (!response.ok) {
            throw new Error('Error al actualizar el profesor');
        }

        return await response.json();
    } catch (error) {
        console.error('Error al actualizar el profesor:', error);
        throw error;
    }
};

const deleteProfesor = async (token, id_profesor) => {
    try {
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.PROFESORES}/${id_profesor}`;
        const response = await authFetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': token
            },
        });

        if (!response.ok) {
            throw new Error('Error al eliminar el profesor');
        }

        return await response.json();
    } catch (error) {
        console.error('Error al eliminar el profesor:', error);
        throw new Error('Error al eliminar el profesor');
    }
};


export const profesoresService = {
    profesores,
    addProfesor,
    deleteProfesor,
    updateProfesor,
};