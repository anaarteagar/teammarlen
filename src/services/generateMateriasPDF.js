import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generateMateriasPDF = (materias, materiasGeneratingReport) => {
    const adminRoleId = '66789c052ed810254bf43470';
    const userRoleId = '66789c052ed810254bf4346f';

    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Materias', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);
    doc.text(`Generado por: ${materiasGeneratingReport}`, 14, 40);

    const headers = [
        ['ID', 'Nombre', 'Descripcion']
    ];

    const rows = materias.map(materia => [
        materia._id,
        materia.nombre,
        materia.descripcion
    ]);

    doc.autoTable({
        head: headers,
        body: rows,
        startY: 50,
    });

    doc.save('ReporteMaterias.pdf');
};

export default generateMateriasPDF;
