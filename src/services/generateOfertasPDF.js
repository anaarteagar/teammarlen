import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generateOfertasPDF = (ofertas, ofertaGeneratingReport) => {
    const adminRoleId = '66789c052ed810254bf43470';
    const userRoleId = '66789c052ed810254bf4346f';

    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Ofertas Educativas', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);
    doc.text(`Generado por: ${ofertaGeneratingReport}`, 14, 40);

    const headers = [
        ['ID', 'Nombre', 'Estado', 'Admisiones']
    ];

    const rows = ofertas.map(oferta => [
        oferta._id,
        oferta.nombre,
        oferta.activo ? 'Activo' : 'Inactivo',
        oferta.admisiones
    ]);

    doc.autoTable({
        head: headers,
        body: rows,
        startY: 50,
    });

    doc.save('ReporteOfertas.pdf');
};

export default generateOfertasPDF;
