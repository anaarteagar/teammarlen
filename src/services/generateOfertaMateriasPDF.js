import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generateOfertaMateriasPDF = (oferta) => {
    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Oferta Educativa', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);

    doc.text(`Oferta Educativa: ${oferta.nombre}`, 14, 40);

    if (oferta.materias.length > 0) {
        const headers = [
            ['Nombre de Materia']
        ];

        const rows = oferta.materias.map(materia => [materia.nombre]);

        doc.autoTable({
            head: headers,
            body: rows,
            startY: 50,
        });
    } else {
        doc.text('No hay materias disponibles para esta oferta.', 14, 50);
    }

    doc.save(`OfertaEducativa_${oferta._id}.pdf`);
};

export default generateOfertaMateriasPDF;
