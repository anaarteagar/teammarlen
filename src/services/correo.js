import { ENV } from '../utils/constants';

export const sendEmail = async (name, email, asunto, mensaje) => {
    try {
        const response = await fetch(`${ENV.API_URL}/${ENV.ENDPOINTS.CORREO}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name,
                email,
                asunto,
                mensaje
            }),
        });

        if (!response.ok) {
            throw new Error('Error al enviar el correo');
        }

        return await response.json();
    } catch (error) {
        console.error('Error al enviar el correo:', error);
        throw new Error('Error al enviar el correo');
    }
};
