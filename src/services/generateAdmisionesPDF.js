import jsPDF from 'jspdf';
import 'jspdf-autotable';

const generateAdmisionesPDF = (admisiones, userGeneratingReport) => {
    const doc = new jsPDF();
    const currentDate = new Date().toLocaleString();

    doc.text('Reporte de Admisiones', 14, 20);
    doc.text(`Fecha de impresión: ${currentDate}`, 14, 30);
    doc.text(`Generado por: ${userGeneratingReport}`, 14, 40);

    const headers = [
        ['ID', 'Nombre', 'Estado', 'Ofertas Educativas']
    ];

    const rows = admisiones.map(admision => [
        admision._id,
        admision.nombre,
        admision.activo ? 'Activa' : 'Inactiva',
        admision.ofertas.map(oferta => oferta ? oferta.nombre : '').join(', ')
    ]);

    doc.autoTable({
        head: headers,
        body: rows,
        startY: 50,
    });

    doc.save('ReporteAdmisiones.pdf');
};

export default generateAdmisionesPDF;
