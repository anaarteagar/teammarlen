export const changeFavicon = (link) => {
    let favicon = document.querySelector('link[rel="icon"]');
    if (favicon) {
        favicon.href = `${link}?v=${new Date().getTime()}`; // Agrega un parámetro de versión para evitar caché
    } else {
        favicon = document.createElement('link');
        favicon.rel = 'icon';
        favicon.href = `${link}?v=${new Date().getTime()}`;
        document.head.appendChild(favicon);
    }
};
