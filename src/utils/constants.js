export const ENV = {
    API_URL: "https://lizard2.vercel.app",
    ENDPOINTS:{
        LOGIN:'api/auth/signin',
        REGISTER:'api/auth/signup',
        USER:'api/users',
        ADMISIONES:'api/admision',
        PROFESORES: 'api/profesores',
        OFERTA: 'api/oferta',
        VINCULAR: 'api/oferadmi',
        DIVISIONES: 'api/divisiones',
        MATERIA: 'api/materia',
        CUATRIMESTRE: 'api/cuatrimestre',
        CORREO: 'api/correo'
    },
    STORAGE: {
        TOKEN: "token",
    }
}
