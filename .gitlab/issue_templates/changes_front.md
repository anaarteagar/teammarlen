---
name: Solicitud de Cambio en el Frontend
about: Propuesta para realizar cambios específicos en la interfaz de usuario
title: "[FRONTEND] Título descriptivo del cambio"
---

**Descripción del Cambio**

Describe claramente el cambio que se propone realizar en el frontend.

**Motivación**

Explica por qué este cambio es necesario y qué problema o mejora busca resolver.

**Requisitos y Alcance**

1. Requisito 1
2. Requisito 2
3. ...

**Diseños y Maquetas**

Adjunta cualquier diseño, maqueta o esquema que ilustre el cambio propuesto.

**Comportamiento Esperado**

Describe cómo debería funcionar y verse el frontend después de realizar el cambio.

**Impacto en la Experiencia del Usuario**

Indica cómo afectará este cambio a la experiencia del usuario y cualquier posible impacto negativo.

**Pasos de Implementación**

1. Paso 1
2. Paso 2
3. ...

**Información Adicional**

Añade cualquier información adicional que consideres relevante, como dependencias, riesgos, notas de implementación, etc.

**Prioridad**

- [ ] Alta
- [ ] Media
- [ ] Baja

**Contexto**

- Sistema operativo:
- Navegador (si aplica):
- Versión de la aplicación (si aplica):
