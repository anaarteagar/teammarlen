---
name: Solicitud de Nueva Característica
about: Propón una nueva característica que mejore el proyecto
title: "[FEATURE] Título descriptivo de la característica"
---

**Descripción de la Característica**

Describe claramente la nueva característica que estás proponiendo.

**Motivación**

Explica por qué esta característica es necesaria y qué problema resuelve.

**Requisitos**

1. Requisito 1
2. Requisito 2
3. ...

**Funcionalidad Esperada**

Describe cómo debería funcionar la nueva característica.

**Impacto**

Indica cómo afectará esta característica al proyecto, usuarios y otras funcionalidades.

**Alternativas Consideradas**

Menciona si consideraste otras soluciones y por qué esta es la mejor opción.

**Capturas de Pantalla/Diseños**

Si es relevante, añade capturas de pantalla o diseños que ilustren la nueva característica.

**Información Adicional**

Añade cualquier información adicional que consideres relevante, como dependencias, riesgos, o notas de implementación.

**Contexto**

- Sistema operativo:
- Navegador (si aplica):
- Versión de la aplicación (si aplica):
