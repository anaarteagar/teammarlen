---
name: Informe de Problema de Diseño
about: Informa sobre un problema relacionado con el diseño para ayudar a los diseñadores a mejorar
title: "[DESIGN] Título descriptivo del problema de diseño"
---

**Descripción del Problema de Diseño**

Describe claramente cuál es el problema de diseño que has encontrado.

**Contexto y Ubicación**

Indica en qué parte del sistema o aplicación se encuentra el problema de diseño.

**Pasos para Reproducir**

1. Paso 1
2. Paso 2
3. ...

**Comportamiento Esperado**

Describe cómo debería ser el diseño correcto o esperado.

**Comportamiento Actual**

Describe cuál es el diseño actual y por qué lo consideras un problema.

**Capturas de Pantalla/Diseños**

Si es relevante, añade capturas de pantalla o diseños que ilustren el problema de diseño.

**Información Adicional**

Añade cualquier información adicional que consideres relevante, como especificaciones de diseño, dispositivos afectados, etc.

**Prioridad**

- [ ] Alta
- [ ] Media
- [ ] Baja

**Contexto**

- Sistema operativo:
- Navegador (si aplica):
- Versión de la aplicación (si aplica):
