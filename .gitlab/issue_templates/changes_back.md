---
name: Solicitud de Cambio en el Backend
about: Propuesta para realizar cambios específicos en la lógica del servidor o base de datos
title: "[BACKEND] Título descriptivo del cambio"
---

**Descripción del Cambio**

Describe claramente el cambio que se propone realizar en el backend.

**Motivación**

Explica por qué este cambio es necesario y qué problema o mejora busca resolver.

**Requisitos y Alcance**

1. Requisito 1
2. Requisito 2
3. ...

**Detalles Técnicos**

Proporciona detalles técnicos específicos sobre el cambio, como la arquitectura afectada, endpoints, esquemas de base de datos, etc.

**Comportamiento Esperado**

Describe cómo debería funcionar el backend después de realizar el cambio.

**Impacto en el Sistema**

Indica cómo afectará este cambio al sistema en general y cualquier posible impacto en otras partes del sistema.

**Pasos de Implementación**

1. Paso 1
2. Paso 2
3. ...

**Información Adicional**

Añade cualquier información adicional que consideres relevante, como dependencias, riesgos, notas de implementación, etc.

**Prioridad**

- [ ] Alta
- [ ] Media
- [ ] Baja

**Contexto**

- Versión del sistema operativo del servidor:
- Versión del lenguaje/framework utilizado:
- Componentes afectados:
